(** This file provides generic "lifting" lemmas to prove weakest preconditions
in the lifted Iron logic, from those of the basic Iron logic. The lifting lemmas
appear in different forms, depending on whether one needs the [perm] connective
in the pre- or postcondition, or both. *)
From iron.iron_logic Require Export weakestpre.
From iris.program_logic Require Import lifting.
From iris.proofmode Require Import proofmode.
Set Default Proof Using "Type".

Section lifting.
Context `{ironG Λ Σ}.
Implicit Types s : stuckness.
Implicit Types v : val Λ.
Implicit Types e : expr Λ.
Implicit Types σ : state Λ.
Implicit Types P Q : ironProp Σ.
Implicit Types Φ : val Λ → ironProp Σ.

Lemma iron_wp_lift s E e Φ π :
  WP e @ s; E {{ v, Φ v π }} -∗ (WP e @ s; E {{ Φ }}) π.
Proof.
  rewrite iron_wp_eq /=. iIntros "Hwp" (π') "Hp". iApply (wp_wand with "Hwp").
  iIntros (v) "HΦ". iExists π', π. by iFrame.
Qed.

Lemma iron_wp_lift_alloc s E e Φ π :
  (∀ π', perm π' -∗ WP e @ s; E {{ v, Φ v (π ⋅ Some π') }}) -∗
  (WP e @ s; E {{ Φ }}) π.
Proof.
  rewrite iron_wp_eq /=. iIntros "Hwp" (π') "[Hp Hp']".
  iSpecialize ("Hwp" with "Hp"). iApply (wp_wand with "Hwp").
  iIntros (v) "HΦ". iExists (π' / 2)%Qp, (π ⋅ Some (π' / 2)%Qp). iFrame.
  by rewrite comm_L -cmra_opM_opM_assoc_L /= frac_op Qp.div_2.
Qed.

Lemma iron_wp_lift_alloc_unit s E e Φ π :
  (∀ π', perm π' -∗ WP e @ s; E {{ v, Φ v (π ⋅ Some π') }}) ⊢
  (WP e @ s; E {{ Φ }}) π.
Proof.
  rewrite iron_wp_eq /=. iIntros "Hwp" (π') "[Hp Hp']".
  iSpecialize ("Hwp" with "Hp"). iApply (wp_wand with "Hwp").
  iIntros (v) "HΦ". iExists (π' / 2)%Qp, (π ⋅ Some (π' / 2)%Qp). iFrame.
  by rewrite comm_L -cmra_opM_opM_assoc_L /= frac_op Qp.div_2.
Qed.

Lemma iron_wp_lift_free s E e Φ π :
  (WP e @ s; E {{ v, ∃ π1 π2, ⌜ π = Some π1 ⋅ π2 ⌝ ∧ perm π1 ∗ Φ v π2 }}) ⊢
  (WP e @ s; E {{ Φ }}) π.
Proof.
  rewrite iron_wp_eq /=. iIntros "Hwp" (π') "Hp". iApply (wp_wand with "Hwp").
  iIntros (v). iDestruct 1 as (π1 π2 ->) "[Hp' HΦ]".
  iExists (π' + π1)%Qp, π2. iFrame. by rewrite -cmra_opM_opM_assoc_L.
Qed.

Lemma iron_wp_lift_free_unit s E e Φ π :
  (WP e @ s; E {{ v, perm π ∗ Φ v ε }}) ⊢
  (WP e @ s; E {{ Φ }}) (Some π).
Proof.
  rewrite -iron_wp_lift_free. f_equiv=> v. iIntros "[Hp HΦ]".
  iExists π, ε. by iFrame.
Qed.

Lemma iron_wp_pure_step_fupd `{Inhabited (state Λ)} s E E' e1 e2 φ n Φ :
  PureExec φ n e1 e2 →
  φ →
  (|={E}[E']▷=>^n <affine> ⎡ £ n ⎤ -∗ WP e2 @ s; E {{ Φ }}) ⊢ WP e1 @ s; E {{ Φ }}.
Proof.
  rewrite iron_wp_eq /=. iStartProof (iProp _). iIntros (Hstep ? π1) "H".
  iIntros (π2) "Hp". iApply (wp_pure_step_fupd _ _ E'); first done.
  generalize (£ n); intros LC.
  clear Hstep. iInduction n as [|n] "IH" forall (π1 π2); simpl.
  { iIntros "Hlc". rewrite fracPred_at_wand. iSpecialize ("H" $! None).
    rewrite right_id_L. iApply ("H" with "[Hlc]"); last done.
    rewrite fracPred_at_affinely fracPred_at_embed. auto. }
  rewrite fracPred_at_fupd. iMod ("H" with "Hp") as (π3 π4 ->) "[Hp H]".
  rewrite fracPred_at_later fracPred_at_fupd. iIntros "!> !>".
  iMod ("H" with "Hp") as (π5 π6 ->) "[Hp H]". iIntros "!>".
  by iApply ("IH" with "H").
Qed.

Lemma iron_wp_pure_step_later `{Inhabited (state Λ)} s E e1 e2 φ n Φ :
  PureExec φ n e1 e2 →
  φ →
  ▷^n WP e2 @ s; E {{ Φ }} ⊢ WP e1 @ s; E {{ Φ }}.
Proof.
  intros Hexec ?. rewrite -iron_wp_pure_step_fupd //. clear Hexec.
  generalize (£ n); intros LC. induction n as [|n IH]; simpl; [by auto|].
  by rewrite -step_fupd_intro // IH.
Qed.
End lifting.
