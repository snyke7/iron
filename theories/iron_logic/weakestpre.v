(** This file defines the weakest precondition connective of the lifted Iron
logic. It does do so by instantiating the [Wp] class so that the notation can
be overloaded (it's disambiguated by the type of pre- and postcondition).

Like the version of weakest preconditions in Iris, we abstract over the actual
programming language that is being used (we reuse Iris's [language]
abstraction). Weakest preconditions are instantiated with an actual language
in the [heap_lang] folder.

Since Iris does not provide a general abstraction for the laws of weakest
preconditions, the structured (i.e. the lemmas) of this file is the same as that
of [program_logic/weakestpre], with the exception of the [iron_] prefixes to
signify that they apply to the Iron version of weakest preconditions. *)
From iron.iron_logic Require Export iron.
From iris.program_logic Require Export weakestpre.
From iris.proofmode Require Import proofmode classes.
Set Default Proof Using "Type".
Import uPred.

Class ironG (Λ : language) (Σ : gFunctors) := IronG {
  iron_invG : invGS Σ;
  iron_iron_invG :: ironInvG Σ;
  iron_state_interp : state Λ → list (observation Λ) → nat → iProp Σ;
  iron_fork_post : val Λ → iProp Σ;
}.

Global Instance iron_irisG `{ironG Λ Σ} : irisGS Λ Σ := {|
  iris_invGS := iron_invG;
  state_interp σ _ := iron_state_interp σ;
  fork_post := iron_fork_post;
  num_laters_per_step _ := 0;
  state_interp_mono _ _ _ _ := fupd_intro _ _;
|}.

Definition iron_wp_def `{ironG Λ Σ} (s : stuckness) (E : coPset)
    (e : expr Λ) (Φ : val Λ → ironProp Σ) : ironProp Σ :=
  FracPred (λ π2, ∀ π1, perm π1 -∗ WP e @ s;E {{ v, ∃ π1' π2',
    ⌜ π1 ⋅? π2 = π1' ⋅? π2' ⌝ ∧ perm π1' ∗ Φ v π2' }})%I.
Definition iron_wp_aux `{ironG Λ Σ} : seal (@iron_wp_def Λ Σ _). by eexists. Qed.
Global Instance iron_wp `{ironG Λ Σ} : Wp (ironProp Σ) (expr Λ) (val Λ) stuckness :=
  iron_wp_aux.(unseal).
Definition iron_wp_eq `{ironG Λ Σ} : wp = @iron_wp_def Λ Σ _ := iron_wp_aux.(seal_eq).

Section wp.
Context `{ironG Λ Σ}.
Implicit Types s : stuckness.
Implicit Types P : ironProp Σ.
Implicit Types Φ : val Λ → ironProp Σ.
Implicit Types v : val Λ.
Implicit Types e : expr Λ.

Global Instance iron_wp_ne s E e n :
  Proper (pointwise_relation _ (@dist (ironProp Σ) _ n) ==> dist n)
    (wp (PROP:=ironProp Σ) s E e).
Proof. rewrite iron_wp_eq. split=>/= π. solve_proper. Qed.
Global Instance iron_wp_proper s E e :
  Proper (pointwise_relation _ (≡@{ironProp Σ}) ==> (≡))
    (wp (PROP:=ironProp Σ)  s E e).
Proof. rewrite iron_wp_eq. split=>/= π. solve_proper. Qed.

Lemma iron_wp_value' s E Φ v : Φ v ⊢ WP of_val v @ s; E {{ Φ }}.
Proof.
  rewrite iron_wp_eq. iStartProof (iProp _); iIntros (π1) "HΦ"; iIntros (π2) "Hp".
  iApply wp_value'. iExists π2, π1. by iFrame.
Qed.
Lemma iron_wp_value_inv' s E Φ v : WP of_val v @ s; E {{ Φ }} ={E}=∗ Φ v.
Proof.
  rewrite iron_wp_eq. iStartProof (iProp _); iIntros (π1) "H"; iIntros (π2) "Hp".
  iSpecialize ("H" with "Hp"). rewrite wp_value_fupd'. done.
Qed.

Lemma iron_wp_strong_mono s1 s2 E1 E2 e Φ Ψ :
  s1 ⊑ s2 → E1 ⊆ E2 →
  WP e @ s1; E1 {{ Φ }} -∗ (∀ v, Φ v ={E2}=∗ Ψ v) -∗ WP e @ s2; E2 {{ Ψ }}.
Proof.
  rewrite iron_wp_eq. iStartProof (iProp _); iIntros (?? π1) "H".
  iIntros (π2) "HΦΨ /=". iIntros (π3) "Hp". iSpecialize ("H" with "Hp").
  iApply (wp_strong_mono with "H"); [done..|]. iIntros (v).
  iDestruct 1 as (π4 π5 Hπ) "[Hp HΦ]". iSpecialize ("HΦΨ" with "HΦ Hp").
  by rewrite -!cmra_opM_opM_assoc_L Hπ !cmra_opM_opM_assoc_L (comm_L _ π5).
Qed.

Lemma iron_fupd_wp s E e Φ : (|={E}=> WP e @ s; E {{ Φ }}) ⊢ WP e @ s; E {{ Φ }}.
Proof.
  rewrite iron_wp_eq. iStartProof (iProp _); iIntros (π1) "H"; iIntros (π2) "Hp /=".
  iApply fupd_wp. iMod ("H" with "Hp") as (π3 π4 ->) "[Hp H]". by iApply "H".
Qed.
Lemma iron_wp_fupd s E e Φ : WP e @ s; E {{ v, |={E}=> Φ v }} ⊢ WP e @ s; E {{ Φ }}.
Proof. iIntros "H". iApply (iron_wp_strong_mono s s E with "H"); auto. Qed.

Lemma iron_wp_atomic s E1 E2 e Φ `{!Atomic (stuckness_to_atomicity s) e} :
  (|={E1,E2}=> WP e @ s; E2 {{ v, |={E2,E1}=> Φ v }}) ⊢ WP e @ s; E1 {{ Φ }}.
Proof.
  rewrite iron_wp_eq. iStartProof (iProp _); iIntros (π1) "H"; iIntros (π2) "Hp /=".
  iApply wp_atomic. iMod ("H" with "Hp") as (π3 π4 ->) "[Hp H]". iModIntro.
  iSpecialize ("H" with "Hp"). iApply (wp_wand with "H").
  iIntros (v). iDestruct 1 as (π5 π6 ->) "[Hp H]". rewrite fracPred_at_fupd.
  by iApply "H".
Qed.

Lemma iron_wp_step_fupd s E1 E2 e P Φ :
  TCEq (to_val e) None → E2 ⊆ E1 →
  (|={E1}[E2]▷=> P) -∗ WP e @ s; E2 {{ v, P ={E1}=∗ Φ v }} -∗ WP e @ s; E1 {{ Φ }}.
Proof.
  rewrite iron_wp_eq. iStartProof (iProp _); iIntros (?? π1) "HP".
  iIntros (π2) "H"; iIntros (π3) "[Hp Hp'] /=".
  iApply (wp_step_fupd with "[HP Hp]"); [done| | ].
  - iMod ("HP" with "Hp") as (π4 π5 Hπ) "[Hp H]". iIntros "!> !>".
    iSpecialize ("H" with "Hp"). rewrite <-Hπ. iApply "H".
  - iSpecialize ("H" with "Hp'"). iApply (wp_wand with "H"); iIntros (v).
    iDestruct 1 as (π4 π5 Hπ) "[Hp HP]". iDestruct 1 as (π6 π7 Hπ') "[Hp' H]".
    iMod ("HP" with "H Hp") as (π8 π9 Hπ'') "[Hp H]".
    iModIntro. iExists (π6 ⋅ π8), π9. iIntros "{$Hp $Hp' $H} !%".
    rewrite cmra_op_opM_assoc_L -Hπ'' -!cmra_opM_opM_assoc_L -Hπ.
    rewrite !cmra_opM_opM_assoc_L -(comm_L _ π7) -!cmra_op_opM_assoc_L (comm_L _ π6).
    rewrite cmra_op_opM_assoc_L -!cmra_opM_opM_assoc_L -Hπ'.
    by rewrite !cmra_opM_opM_assoc_L -cmra_op_opM_assoc_L frac_op Qp.div_2.
Qed.

Lemma iron_wp_bind K `{!LanguageCtx K} s E e Φ :
  WP e @ s; E {{ v, WP K (of_val v) @ s; E {{ Φ }} }} ⊢ WP K e @ s; E {{ Φ }}.
Proof.
  rewrite iron_wp_eq. iStartProof (iProp _); iIntros (π1) "H"; iIntros (π2) "Hp /=".
  iApply wp_bind. iSpecialize ("H" with "Hp"). iApply (wp_wand with "H").
  iIntros (v). iDestruct 1 as (π5 π6 ->) "[Hp H]". by iApply "H".
Qed.

Lemma iron_wp_bind_inv K `{!LanguageCtx K} s E e Φ :
  WP K e @ s; E {{ Φ }} ⊢ WP e @ s; E {{ v, WP K (of_val v) @ s; E {{ Φ }} }}.
Proof.
  rewrite iron_wp_eq. iStartProof (iProp _).
  iIntros (π1) "H"; iIntros (π2) "[Hp Hp'] /=".
  iSpecialize ("H" with "Hp"). iPoseProof (wp_bind_inv with "H") as "H".
  iApply (wp_wand with "H"). iIntros (v) "H".
  iExists (π2 / 2)%Qp, (Some (π2 / 2) ⋅ π1)%Qp.
  iSplit; first by rewrite /= -cmra_opM_opM_assoc_L /= frac_op Qp.div_2.
  iIntros "{$Hp'}" (π3) "Hp". iApply (wp_wand with "H").
  iIntros (w). rewrite -cmra_opM_opM_assoc_L /= cmra_op_opM_assoc_L.
  iDestruct 1 as (π4 π5 Hπ) "[??]". rewrite Hπ. iExists (π3 ⋅ π4), π5.
  rewrite -cmra_op_opM_assoc_L. by iFrame.
Qed.

(** * Derived rules *)
Lemma iron_wp_mono s E e Φ Ψ : (∀ v, Φ v ⊢ Ψ v) → WP e @ s; E {{ Φ }} ⊢ WP e @ s; E {{ Ψ }}.
Proof.
  iIntros (HΦ) "H"; iApply (iron_wp_strong_mono with "H"); auto.
  iIntros (v) "?". by iApply HΦ.
Qed.
Lemma iron_wp_stuck_mono s1 s2 E e Φ :
  s1 ⊑ s2 → WP e @ s1; E {{ Φ }} ⊢ WP e @ s2; E {{ Φ }}.
Proof. iIntros (?) "H". iApply (iron_wp_strong_mono with "H"); auto. Qed.
Lemma iron_wp_stuck_weaken s E e Φ :
  WP e @ s; E {{ Φ }} ⊢ WP e @ E ?{{ Φ }}.
Proof. apply iron_wp_stuck_mono. by destruct s. Qed.
Lemma iron_wp_mask_mono s E1 E2 e Φ : E1 ⊆ E2 → WP e @ s; E1 {{ Φ }} ⊢ WP e @ s; E2 {{ Φ }}.
Proof. iIntros (?) "H"; iApply (iron_wp_strong_mono with "H"); auto. Qed.
Global Instance iron_wp_mono' s E e :
  Proper (pointwise_relation _ (⊢) ==> (⊢)) (wp (PROP:=ironProp Σ) s E e).
Proof. by intros Φ Φ' ?; apply iron_wp_mono. Qed.

Lemma iron_wp_value s E Φ e v : IntoVal e v → Φ v ⊢ WP e @ s; E {{ Φ }}.
Proof. intros <-. by apply iron_wp_value'. Qed.
Lemma iron_wp_value_fupd' s E Φ v : (|={E}=> Φ v) ⊢ WP of_val v @ s; E {{ Φ }}.
Proof. intros. by rewrite -iron_wp_fupd -iron_wp_value'. Qed.
Lemma iron_wp_value_fupd s E Φ e v `{!IntoVal e v} :
  (|={E}=> Φ v) ⊢ WP e @ s; E {{ Φ }}.
Proof. intros. rewrite -iron_wp_fupd -iron_wp_value //. Qed.
Lemma iron_wp_value_inv s E Φ e v : IntoVal e v → WP e @ s; E {{ Φ }} ={E}=∗ Φ v.
Proof. intros <-. by apply iron_wp_value_inv'. Qed.

Lemma iron_wp_frame_l s E e Φ R : R ∗ WP e @ s; E {{ Φ }} ⊢ WP e @ s; E {{ v, R ∗ Φ v }}.
Proof. iIntros "[? H]". iApply (iron_wp_strong_mono with "H"); auto with iFrame. Qed.
Lemma iron_wp_frame_r s E e Φ R : WP e @ s; E {{ Φ }} ∗ R ⊢ WP e @ s; E {{ v, Φ v ∗ R }}.
Proof. iIntros "[H ?]". iApply (iron_wp_strong_mono with "H"); auto with iFrame. Qed.

Lemma iron_wp_frame_step_l s E1 E2 e Φ R :
  TCEq (to_val e) None → E2 ⊆ E1 →
  (|={E1}[E2]▷=> R) ∗ WP e @ s; E2 {{ Φ }} ⊢ WP e @ s; E1 {{ v, R ∗ Φ v }}.
Proof.
  iIntros (??) "[Hu Hwp]". iApply (iron_wp_step_fupd with "Hu"); try done.
  iApply (iron_wp_mono with "Hwp"). by iIntros (?) "$$ !>".
Qed.
Lemma iron_wp_frame_step_r s E1 E2 e Φ R :
  TCEq (to_val e) None → E2 ⊆ E1 →
  WP e @ s; E2 {{ Φ }} ∗ (|={E1}[E2]▷=> R) ⊢ WP e @ s; E1 {{ v, Φ v ∗ R }}.
Proof.
  rewrite [(WP _ @ _; _ {{ _ }} ∗ _)%I]comm; setoid_rewrite (comm _ _ R).
  apply iron_wp_frame_step_l.
Qed.
Lemma iron_wp_frame_step_l' s E e Φ R :
  TCEq (to_val e) None → ▷ R ∗ WP e @ s; E {{ Φ }} ⊢ WP e @ s; E {{ v, R ∗ Φ v }}.
Proof. iIntros (?) "[??]". iApply (iron_wp_frame_step_l s E E); try iFrame; eauto. Qed.
Lemma iron_wp_frame_step_r' s E e Φ R :
  TCEq (to_val e) None → WP e @ s; E {{ Φ }} ∗ ▷ R ⊢ WP e @ s; E {{ v, Φ v ∗ R }}.
Proof. iIntros (?) "[??]". iApply (iron_wp_frame_step_r s E E); try iFrame; eauto. Qed.

Lemma iron_wp_wand s E e Φ Ψ :
  WP e @ s; E {{ Φ }} -∗ (∀ v, Φ v -∗ Ψ v) -∗ WP e @ s; E {{ Ψ }}.
Proof.
  iIntros "Hwp H". iApply (iron_wp_strong_mono with "Hwp"); auto.
  iIntros (?) "?". by iApply "H".
Qed.
Lemma iron_wp_wand_l s E e Φ Ψ :
  (∀ v, Φ v -∗ Ψ v) ∗ WP e @ s; E {{ Φ }} ⊢ WP e @ s; E {{ Ψ }}.
Proof. iIntros "[H Hwp]". iApply (iron_wp_wand with "Hwp H"). Qed.
Lemma iron_wp_wand_r s E e Φ Ψ :
  WP e @ s; E {{ Φ }} ∗ (∀ v, Φ v -∗ Ψ v) ⊢ WP e @ s; E {{ Ψ }}.
Proof. iIntros "[Hwp H]". iApply (iron_wp_wand with "Hwp H"). Qed.
End wp.

(** Proofmode class instances *)
Section proofmode_classes.
  Context `{ironG Λ Σ}.
  Implicit Types P Q : ironProp Σ.
  Implicit Types Φ : val Λ → ironProp Σ.

  Global Instance frame_iron_wp p s E e R Φ Ψ :
    (∀ v, Frame p R (Φ v) (Ψ v)) →
    Frame p R (WP e @ s; E {{ Φ }}) (WP e @ s; E {{ Ψ }}).
  Proof. rewrite /Frame=> HR. rewrite iron_wp_frame_l. apply iron_wp_mono, HR. Qed.

  Global Instance is_except_0_iron_wp s E e Φ : IsExcept0 (WP e @ s; E {{ Φ }}).
  Proof. by rewrite /IsExcept0 -{2}iron_fupd_wp -except_0_fupd -fupd_intro. Qed.

  Global Instance elim_modal_bupd_iron_wp s E e P Φ :
    ElimModal True false false (|==> P) P (WP e @ s; E {{ Φ }}) (WP e @ s; E {{ Φ }}).
  Proof.
    by rewrite /ElimModal intuitionistically_if_elim
      (bupd_fupd E) fupd_frame_r wand_elim_r iron_fupd_wp.
  Qed.

  Global Instance elim_modal_bupd_iron_wp_affine s E e P P' Φ :
    FromAffinely P' P →
    ElimModal True true false (|==> P) P' (WP e @ s; E {{ Φ }}) (WP e @ s; E {{ Φ }}).
  Proof.
    rewrite /FromAffinely /ElimModal /= => <- _.
    rewrite bi.intuitionistically_affinely fracPred_affinely_bupd.
    by rewrite (bupd_fupd E) fupd_frame_r wand_elim_r iron_fupd_wp.
  Qed.

  Global Instance elim_modal_fupd_iron_wp p s E e P Φ :
    ElimModal True p false (|={E}=> P) P (WP e @ s; E {{ Φ }}) (WP e @ s; E {{ Φ }}).
  Proof.
    by rewrite /ElimModal intuitionistically_if_elim
      fupd_frame_r wand_elim_r iron_fupd_wp.
  Qed.

  Global Instance elim_modal_fupd_iron_iron_wp_atomic p s E1 E2 e P Φ :
    Atomic (stuckness_to_atomicity s) e →
    ElimModal True p false (|={E1,E2}=> P) P
            (WP e @ s; E1 {{ Φ }}) (WP e @ s; E2 {{ v, |={E2,E1}=> Φ v }})%I.
  Proof.
    intros. by rewrite /ElimModal intuitionistically_if_elim
      fupd_frame_r wand_elim_r iron_wp_atomic.
  Qed.

  Global Instance add_modal_fupd_iron_wp s E e P Φ :
    AddModal (|={E}=> P) P (WP e @ s; E {{ Φ }}).
  Proof. by rewrite /AddModal fupd_frame_r wand_elim_r iron_fupd_wp. Qed.
End proofmode_classes.
