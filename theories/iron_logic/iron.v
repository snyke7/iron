(** This file defines the main constructs of the Iron logic. It does do so in
two stages:

1. It defines the [ironInvG] class that expresses the cameras that are needed
   to get the Iron reasoning principles in the basic Iron logic.
2. It defines the type [ironProp] of propositions of the Iron++ logic.
 
The latter is done by instantiating the general construction of predicates over
fractions (as in the file [bi/fracpred]) with a concrete BI: that of Iris
propositions, i.e:

<<<
Notation ironProp Σ := (fracPred (iProp Σ)).
>>>

From the generic [fracPred] construction we get all of the standard BI
connectives, but this file defines some notions that are missing:

- The [perm π] connective, which is denoted [𝖊(π)] in the paper.
- The type class [Uniform], which expresses that an Iron++ proposition is
  uniform w.r.t fractions. We provide instances showing that uniform
  propositions are closed under affine propositions, (big) separating
  conjunction, disjunctions, and existential quantifiers.
- A lifted version of fancy updates [|={E1,E2}=>] that threads through the
  [perm π] connective. This is done by instantiating Iris's [BiFUpdMixin] type
  class so that we get overloaded notations, and can share common derived
  results. *)
From iron.bi Require Export fracpred.
From iris.bi.lib Require Export fractional.
From iris.base_logic.lib Require Export fancy_updates invariants.
From iris.base_logic.lib Require Export cancelable_invariants.
From iris.algebra Require Import lib.frac_auth.
From iris.algebra Require Import ufrac.
From iris.proofmode Require Import proofmode.
From iron.proofmode Require Import fracpred.

Class ironInvG (Σ : gFunctors) := IronInvG {
  perm : frac → iProp Σ;
  perm_fractional :: Fractional perm;
  perm_timeless π :: Timeless (perm π);
  fcinv_cinvG :: cinvG Σ;
  fcinv_inG :: inG Σ (frac_authR ufracR);
}.
Notation ironProp Σ := (fracPred (iProp Σ)).
Notation ironPropO Σ := (fracPredO (iPropO Σ)).
Notation ironPropI Σ := (fracPredI (uPredI (iResUR Σ))).

Global Instance perm_as_fractional `{ironInvG Σ} π : AsFractional (perm π) perm π.
Proof. split. done. apply _. Qed.
Global Instance frame_perm `{ironInvG Σ} p π1 π2 π :
  FrameFractionalQp π1 π2 π →
  Frame p (perm π1) (perm π2) (perm π) | 5.
Proof. apply: frame_fractional. Qed.

Class Uniform `{ironInvG Σ} (P : ironProp Σ) :=
  uniform π1 π2 : P (Some (π1 ⋅ π2)) ⊣⊢ P (Some π1) ∗ perm π2.
Arguments Uniform {_ _} _%I.
Global Hint Mode Uniform + - ! : typeclass_instances.
Global Instance: Params (@Uniform) 2 := {}.

Class ExistPerm `{ironInvG Σ} (P : ironProp Σ) :=
  exist_perm : P None ⊢ False.
Arguments ExistPerm {_ _} _%I.
Global Hint Mode ExistPerm + - ! : typeclass_instances.
Global Instance: Params (@ExistPerm) 2 := {}.

Definition iron_fupd_def `{ironInvG Σ, invGS Σ} (E1 E2 : coPset)
    (P : ironProp Σ) : ironProp Σ := FracPred (λ π2, ∀ π1,
  perm π1 ={E1,E2}=∗
    ∃ π1' π2', ⌜ π1 ⋅? π2 = π1' ⋅? π2' ⌝ ∧ perm π1' ∗ P π2')%I.
Definition iron_fupd_aux `{ironInvG Σ, invGS Σ} : seal iron_fupd_def. by eexists. Qed.
Definition iron_fupd `{ironInvG Σ, invGS Σ} : FUpd _ := iron_fupd_aux.(unseal).
Definition iron_fupd_eq `{ironInvG Σ, invGS Σ} : @fupd _ iron_fupd = _ :=
  iron_fupd_aux.(seal_eq).

Section iron.
Context `{ironInvG Σ, invGS Σ}.
Implicit Types P : ironProp Σ.

(** Fancy updates *)
Lemma iron_fupd_mixin : BiFUpdMixin (ironPropI Σ) iron_fupd.
Proof.
  split; rewrite iron_fupd_eq.
  - split=>/= π. solve_proper.
  - intros E1 E2 HE12. split=>/= π2. apply bi.forall_intro=> π1.
    rewrite -(bi.exist_intro π1) -(bi.exist_intro π2) bi.pure_True // left_id.
    apply bi.wand_intro_l. rewrite (fupd_mask_intro_subseteq E1 E2 (fracPred_emp π2)) // fupd_frame_l.
    do 2 f_equiv. apply bi.forall_intro=> π1'.
    rewrite -(bi.exist_intro π1') -(bi.exist_intro π2) bi.pure_True // left_id.
    apply bi.wand_intro_l. by rewrite fupd_frame_l.
  - intros E1 E2 P. split=>/= π2. rewrite fracPred_at_except_0 /=.
    apply bi.forall_intro=> π1. apply bi.wand_intro_l.
    by rewrite (bi.forall_elim π1) bi.except_0_frame_l bi.wand_elim_r except_0_fupd.
  - intros E1 E2 P Q HPQ. split=>/= π2. by setoid_rewrite HPQ.
  - intros E1 E2 E3 P. split=>/= π2. f_equiv=> π1. f_equiv.
    rewrite -(fupd_trans E1 E2 E3). f_equiv.
    apply bi.exist_elim=> π1'; apply bi.exist_elim=> π2'. apply bi.pure_elim_l=> ->.
    by rewrite (bi.forall_elim π1') bi.wand_elim_r.
  - intros E1 E2 Ef P HE1f. split=>/= π2. f_equiv=> π1. f_equiv.
    rewrite -fupd_mask_frame_r' //. f_equiv. apply bi.impl_intro_l, bi.pure_elim_l=> ?.
    f_equiv=> π1'; f_equiv=> π2'. by rewrite (bi.pure_True (_ ## _)) // left_id.
  - intros E1 E2 P Q. split=>/= π2. rewrite fracPred_at_sep /=.
    apply bi.exist_elim=> π21. apply bi.exist_elim=> π22. apply bi.pure_elim_l=> ->.
    apply bi.forall_intro=> π1. apply bi.wand_intro_l.
    rewrite (bi.forall_elim π1) assoc bi.wand_elim_r fupd_frame_r.
    repeat setoid_rewrite bi.sep_exist_r. f_equiv. f_equiv=> π1'.
    apply bi.exist_elim=> π2'. rewrite -(bi.exist_intro (π2' ⋅ π22)).
    rewrite -!bi.persistent_and_sep_assoc -!cmra_opM_opM_assoc_L.
    apply bi.pure_elim_l=> ->. rewrite bi.pure_True // left_id.
    by rewrite -assoc -fracPred_at_sep_2.
Qed.
Global Instance iron_bi_fupd : BiFUpd (ironPropI Σ) :=
  {| bi_fupd_mixin := iron_fupd_mixin |}.

Lemma fracPred_at_fupd π2 E1 E2 P :
  (|={E1,E2}=> P) π2 ⊣⊢ ∀ π1, perm π1 ={E1,E2}=∗
    ∃ π1' π2', ⌜ π1 ⋅? π2 = π1' ⋅? π2' ⌝ ∧ perm π1' ∗ P π2'.
Proof. by rewrite iron_fupd_eq. Qed.
Lemma fracPred_at_fupd_2 π2 E1 E2 P : (|={E1,E2}=> P π2) ⊢ (|={E1,E2}=> P) π2.
Proof.
  rewrite fracPred_at_fupd. apply bi.forall_intro=> π1. apply bi.wand_intro_l.
  rewrite -(bi.exist_intro π1) -(bi.exist_intro π2) bi.pure_True // left_id.
  by rewrite fupd_frame_l.
Qed.

Global Instance iron_bi_bupd_fupd : BiBUpdFUpd (ironPropI Σ).
Proof.
  intros E P. split=>/= π2. rewrite fracPred_at_bupd -fracPred_at_fupd_2 /=.
  by rewrite bupd_fupd.
Qed.

Global Instance make_fracPred_at_fupd E1 E2 π2 P Φ :
  (∀ π, MakeFracPredAt π P (Φ π)) →
  MakeFracPredAt π2 (|={E1,E2}=> P) (∀ π1,
    perm π1 ={E1,E2}=∗ ∃ π1' π2', ⌜ π1 ⋅? π2 = π1' ⋅? π2' ⌝ ∧ perm π1' ∗ Φ π2')%I.
Proof. rewrite /MakeFracPredAt fracPred_at_fupd=> HP. by setoid_rewrite HP. Qed.

(* Higher precendence than elim_modal_bupd_fupd *)
Global Instance elim_modal_bupd_fupd_affine E1 E2 P P' Q :
  FromAffinely P' P →
  ElimModal True true false (|==> P) P' (|={E1,E2}=> Q) (|={E1,E2}=> Q) | 0.
Proof.
  rewrite /FromAffinely /ElimModal /= => <- _.
  rewrite bi.intuitionistically_affinely fracPred_affinely_bupd.
  by rewrite bupd_frame_r bi.wand_elim_r (bupd_fupd E1) fupd_trans.
Qed.

(* TODO: The class [BiEmbedFUpd PROP fracPredSI] holds only in one direction,
we could declare proof mode instance manually for the direction that holds,
but so far, that did not appear to be useful. *)

(** Instances of the Uniform type class*)

(* The following lemma is not an instance, to avoid excessive backtracking it
should only be used at the leaves of type class search. Hence, it's declared as
a [Hint Immediate] in the end of this file. *)
Lemma affine_uniform P : Affine P → Uniform P.
Proof.
  intros ? π1 π2. apply (anti_symm _).
  - rewrite {1}(affine P) fracPred_at_emp bi.pure_False // bi.affinely_False.
    apply bi.False_elim.
  - rewrite {1}(affine P) fracPred_at_emp bi.pure_False // bi.affinely_False.
    rewrite left_absorb. apply bi.False_elim.
Qed.

Global Instance Uniform_proper : Proper ((⊣⊢) ==> iff) Uniform.
Proof. intros P1 P2 HP; split=> ? π π'. by rewrite -HP. by rewrite HP. Qed.

Global Instance False_uniform : Uniform False.
Proof. apply affine_uniform, _. Qed.
Global Instance emp_uniform : Uniform emp.
Proof. by apply affine_uniform. Qed.
Global Instance sep_uniform P1 P2 : Uniform P1 → Uniform P2 → Uniform (P1 ∗ P2).
Proof.
  rewrite /Uniform=> HP1 HP2 π1 π2. rewrite !fracPred_at_sep. apply (anti_symm _).
  - apply bi.exist_elim=> -[π1'|]; apply bi.exist_elim=> -[π2'|];
      apply bi.pure_elim_l; rewrite ?(inj_iff Some) //.
    + intros (π'&π''&π'''&π''''&<-&<-&<-&<-)%Qp.cross_split.
      rewrite HP1 HP2 -(bi.exist_intro (Some π')) -(bi.exist_intro (Some π'')).
      rewrite bi.pure_True // left_id fractional.
      rewrite -!assoc. apply bi.sep_mono_r.
      rewrite !assoc. apply bi.sep_mono_l. rewrite comm //.
    + intros <-. rewrite HP1 -(bi.exist_intro (Some π1)) -(bi.exist_intro ε).
      rewrite bi.pure_True // left_id.
      rewrite -!assoc. apply bi.sep_mono_r. rewrite comm //.
    + intros <-. rewrite HP2 -(bi.exist_intro ε) -(bi.exist_intro (Some π1)).
      rewrite bi.pure_True // left_id. rewrite !assoc //.
  - repeat setoid_rewrite bi.sep_exist_r.
    apply bi.exist_elim=> π1'; apply bi.exist_elim=> π2'.
    rewrite -bi.persistent_and_sep_assoc; apply bi.pure_elim_l.
    destruct π1' as [π1'|], π2' as [π2'|]; rewrite ?(inj_iff Some) // => ->.
    + rewrite -(bi.exist_intro (Some π1')) -(bi.exist_intro (Some (π2' ⋅ π2))).
      rewrite -assoc_L bi.pure_True // left_id HP2. rewrite !assoc //.
    + rewrite -(bi.exist_intro (Some (π1' ⋅ π2))) -(bi.exist_intro ε).
      rewrite right_id_L bi.pure_True // left_id HP1.
      rewrite -!assoc. apply bi.sep_mono_r. rewrite comm //.
    + rewrite -(bi.exist_intro ε) -(bi.exist_intro (Some (π2' ⋅ π2))).
      rewrite bi.pure_True // left_id HP2. rewrite !assoc //.
Qed.
Global Instance big_sepL_uniform {A} (Φ : A → ironProp Σ) l :
  (∀ x, Uniform (Φ x)) → Uniform ([∗ list] x ∈ l, Φ x).
Proof. intros; induction l; simpl; apply _. Qed.
Global Instance exist_uniform {A} (Φ : A → fracPred _) :
  (∀ x, Uniform (Φ x)) → Uniform (∃ x, Φ x).
Proof.
  rewrite /Uniform=> HΦ π1 π2. rewrite !fracPred_at_exist bi.sep_exist_r.
  by setoid_rewrite HΦ.
Qed.
Global Instance or_uniform P1 P2 : Uniform P1 → Uniform P2 → Uniform (P1 ∨ P2).
Proof. intros ??. rewrite bi.or_alt. by apply exist_uniform=> -[]. Qed.

Global Instance affinely_uniform P : Uniform (<affine> P).
Proof. apply affine_uniform, _. Qed.
Global Instance intuitionistically_uniform P : Uniform (□ P).
Proof. apply affine_uniform, _. Qed.
Global Instance and_uniform_l P1 P2 :
  Uniform P1 → Persistent P2 → Absorbing P2 → Uniform (P1 ∧ P2).
Proof. intros. rewrite bi.persistent_and_affinely_sep_r. apply _. Qed.
Global Instance and_uniform_r P1 P2 :
  Persistent P1 → Absorbing P1 → Uniform P2 → Uniform (P1 ∧ P2).
Proof. intros. rewrite bi.persistent_and_affinely_sep_l. apply _. Qed.

(** ExistPerm *)
Global Instance ExistPerm_proper : Proper ((⊣⊢) ==> iff) ExistPerm.
Proof. solve_proper. Qed.

Global Instance False_exist_perm : ExistPerm False.
Proof. by rewrite /ExistPerm fracPred_at_pure. Qed.
Global Instance sep_exist_perm_l P1 P2 : ExistPerm P1 → ExistPerm (P1 ∗ P2).
Proof.
  rewrite /ExistPerm=> HP. rewrite fracPred_at_sep.
  apply bi.exist_elim=> -[π1'|]; apply bi.exist_elim=> -[π2'|];
    apply bi.pure_elim_l=> ? //.
  by rewrite HP left_absorb.
Qed.
Global Instance sep_exist_perm_r P1 P2 : ExistPerm P2 → ExistPerm (P1 ∗ P2).
Proof. rewrite comm. apply _. Qed.
Global Instance exist_exist_perm {A} (Φ : A → fracPred _) :
  (∀ x, ExistPerm (Φ x)) → ExistPerm (∃ x, Φ x).
Proof.
  rewrite /ExistPerm=> HΦ. rewrite !fracPred_at_exist.
  apply bi.exist_elim=> x. by rewrite HΦ.
Qed.
Global Instance or_exist_perm P1 P2 : ExistPerm P1 → ExistPerm P2 → ExistPerm (P1 ∨ P2).
Proof. intros ??. rewrite bi.or_alt. by apply exist_exist_perm=> -[]. Qed.

Global Instance and_exist_perm_l P1 P2 : ExistPerm P1 → ExistPerm (P1 ∧ P2).
Proof. rewrite /ExistPerm=> HP. by rewrite fracPred_at_and HP left_absorb. Qed.
Global Instance and_exist_perm_r P1 P2 : ExistPerm P2 → ExistPerm (P1 ∧ P2).
Proof. rewrite comm; apply _. Qed.

Global Instance from_assumption_fracPred_None p P QQ :
  ExistPerm P → FromAssumption p (P None) QQ.
Proof.
  rewrite /ExistPerm /FromAssumption=> ->.
  by rewrite bi.intuitionistically_if_False bi.False_elim.
Qed.
End iron.

Global Hint Immediate affine_uniform : typeclass_instances.
