(* message_passing_example.v: In this file we have verified the example of our
 * message passing module that we gave in the paper. The actual program is given
 * in [program] and its specification that it does not leak is given in [program_spec].
 *)
From iris.heap_lang Require Export notation.
From iron.heap_lang Require Import proofmode lib.message_passing lib.par lib.list.
Set Default Proof Using "Type*".

Definition left_branch : val := λ: "en",
  let: "l1" := mkList #5 in
  let: "l2" := mkList #10 in
  send "en" "l1";;
  send "en" "l2";;
  close "en".

Definition right_branch : val := λ: "de",
  let: "l3" := mkList #5 in
  send "de" "l3";;
  let: "l4" := receive "de" in
  delete_all (λ: "_", #())%V "l4";;
  close "de".

Definition program : val := λ: "d",
  let: "ch" := new_channel "d" in
  let: "e1" := Fst "ch" in
  let: "e2" := Snd "ch" in
  (left_branch "e1" ||| right_branch "e2").

Section spec.
  Context `{heapGS Σ, queueG Σ, binary_choiceG Σ, spawnG Σ}.
  Definition Ψ' (u : val) : ironProp Σ := (∃ n : Z, <affine> ⌜u = #n⌝)%I.
  Definition Ψ (u : val) : ironProp Σ := (∃ vs, is_list Ψ' u vs)%I.
  Let N := (nroot .@ "ex").

  Instance Ψ_uniform u : Uniform (Ψ u).
  Proof. apply _. Qed.

  Theorem program_spec :
    {{{ emp }}}
      program (λ: "l", delete_all (λ: "_", #())%V "l")%V
    {{{ v, RET v; emp }}}.
  Proof.
    iIntros (Φ) "_ HΦ".
    wp_lam.
    wp_apply (new_channel_spec N Ψ); eauto.
    { iIntros (u); iModIntro; iIntros (Φ') "HΨ HΦ'".
      wp_lam.
      iDestruct "HΨ" as (vs) "Hlist"; eauto.
      wp_apply (delete_all_spec Ψ' u _ vs with "[] [Hlist]"); eauto.
      { iIntros (u'); iModIntro; iIntros (Φ'') "HΨ' HΦ'".
        wp_lam. by iApply "HΦ'". } }
    iIntros (px py γ) "[Hex Hey]".
    wp_let. wp_proj. wp_let. wp_proj. wp_let.
    wp_apply (par_spec (λ _, emp%I) (λ _, emp%I) with "[Hex] [Hey]").
    - wp_lam. wp_lam.
      wp_apply (mkList_spec Ψ' 5%nat); auto.
      { iModIntro; iIntros (m); by iExists _. }
      iIntros (l1) "Hl1". wp_let.
      wp_apply (mkList_spec Ψ' 10%nat); auto.
      { iModIntro; iIntros (m); by iExists _. }
      iIntros (l2) "Hl2". wp_let.
      wp_apply (send_x_spec N Ψ γ px l1 with "[Hex Hl1]").
      { iFrame. iExists (mkList_ref 5); eauto. }
      iIntros "Hex ". wp_seq.

      wp_apply (send_x_spec N Ψ γ px l2 with "[Hex Hl2]").
      { iFrame; iExists _; iFrame. }
      iIntros "Hex". wp_seq.

      wp_apply (close_x_spec with "Hex"); eauto.
    - wp_pures. wp_lam. wp_apply (mkList_spec Ψ' 5%nat); auto.
      { iModIntro; iIntros (m); by iExists _. }
      iIntros (l3) "Hl3". wp_let.

      wp_apply (send_y_spec N Ψ γ py l3 with "[Hey Hl3]").
      { iFrame. iExists (mkList_ref 5); eauto. }
      iIntros "Hey". wp_seq.

      wp_apply (receive_y_spec N Ψ γ py with "Hey"). iIntros (x) "[Hey HΨ']".
      wp_let.
      iDestruct "HΨ'" as (vs) "Hlist".
      wp_apply (delete_all_spec Ψ' x _ vs with "[] [Hlist]"); eauto.
      { iIntros (u'); iModIntro; iIntros (Φ'') "HΨ' HΦ'".
        wp_lam. by iApply "HΦ'". }
      iIntros (v) "_".
      wp_apply (close_y_spec with "Hey"). by iIntros "_ /=".
    - iIntros (v1 v2) "[_ _] !>".
      by iApply "HΦ".
  Qed.
End spec.
