From iris.heap_lang Require Import notation.
From iron.heap_lang Require Import proofmode.

Definition core : val :=
  λ: "ℓ", if: FAA "ℓ" #1 = #1 then Free "ℓ" else #().

Definition thunk : val :=
  λ: <>, let: "ℓ" := ref #0 in Fork (core "ℓ") ;; core "ℓ".

Section proof.
  Context `{!cinvG Σ, !heapGS Σ}.

  Definition core_inv (γ : gname) (ℓ : loc) (π : frac) : iProp Σ :=
    (  (ℓ ↦ #0) (Some π)
     ∨ (ℓ ↦ #1) (Some (π/2))%Qp ∗ cinv_own γ (1/2))%I.

  Lemma core_spec N γ ℓ π :
    cinv N γ (core_inv γ ℓ π) -∗
    {{{ cinv_own γ (1 / 2) }}} core #ℓ {{{ RET #(); perm (π/2) }}}.
  Proof.
    iIntros "#Hinv !#" (Φ) "Hown Hcont". wp_lam.
    wp_bind (FAA _ _).
    iMod (cinv_acc_strong with "Hinv Hown")
      as "([Hℓ | [Hℓ Hown']] & Hown & Hclose)"; first set_solver.
    - wp_apply (wp_faa with "Hℓ").
      rewrite -{4}(Qp.div_2 π) Some_op pointsto_uniform.
      iIntros "[Hpt Hperm]".
      iMod ("Hclose" with "[Hpt Hown]") as "_".
      { iLeft; iRight; iFrame. }
      rewrite -union_difference_L //.
      iModIntro. wp_op; wp_if. by iApply "Hcont".
    - wp_apply (wp_faa with "Hℓ").
      iIntros "Hpt".
      iMod ("Hclose" with "[Hown Hown']") as "_".
      { iRight. iCombine "Hown Hown'" as "$". }
      rewrite -union_difference_L //.
      iModIntro. wp_op. wp_if.
      by wp_apply (wp_free with "Hpt").
  Qed.

  (* Specification in the Iron logic. *)
  Lemma thunk_spec_unlifted π :
    {{{ perm π }}} thunk #() {{{ RET #(); perm π }}}.
  Proof.
    iIntros (Φ) "Hperm Hcont". wp_lam.
    wp_apply (wp_alloc with "Hperm").
    iIntros (ℓ) "Hℓ". wp_let.
    iMod (cinv_alloc_strong (λ _, True) ⊤ nroot) as (γ ?) "[[Hown1 Hown2] Hvs]".
    { apply pred_infinite_True. }
    iMod ("Hvs" $! (core_inv γ ℓ π) with "[$Hℓ]") as "#Hcinv".
    wp_bind (Fork _).
    wp_apply (wp_fork _ _ _ (Some (π / 2)%Qp) with "[Hcont Hown1] [Hown2]").
    - iIntros "!> Hperm".
      wp_seq.
      wp_apply (core_spec with "Hcinv Hown1").
      iIntros "Hperm'". iApply "Hcont". by iCombine "Hperm" "Hperm'" as "$".
    - iNext.
      wp_apply (core_spec with "Hcinv Hown2"). auto.
  Qed.

  (* Specification in the Iron++ logic. *)
  Lemma thunk_spec :
    {{{ emp }}} thunk #() {{{ RET #(); emp }}}.
  Proof.
    (* TODO: make this proof shorter *)
    iStartProof (iProp Σ). iIntros (Φ ? ->) ""; iIntros (πf) "Hcont".
    rewrite iron_wp_eq /=. iIntros (π') "Hp".
    wp_apply (thunk_spec_unlifted with "Hp"). iIntros "Hp".
    iExists π', πf. iSplit; first by rewrite left_id_L. iFrame "Hp".
    rewrite -{2}(left_id_L ε op πf). by iApply "Hcont".
  Qed.
End proof.
