(* spin_lock_track.v: This file contains a verification of the tracked spin-lock
 * implementation described in the paper. It is designed so that it is possible to
 * enforce that all acquired locks are freed in the specification.
 *
 * The specification of the lock is proven in the [proof] section and a proof that
 * every program {{ is_lock ... ∗ unlocked ... 1 }} e {{ unlocked ... 1 }} releases
 * the lock is given as a corollary of adequacy in [adequacy_proofs].
 *)
From iris.heap_lang Require Export lang notation.
From iron.heap_lang Require Import adequacy proofmode.
From iron.iron_logic Require Import fcinv.
From iris.algebra Require Import auth excl.
Set Default Proof Using "Type".

Definition new_lock : val := λ: <>, ref #false.
Definition try_acquire : val := λ: "l", CAS "l" #false #true.
Definition acquire : val :=
  rec: "acquire" "l" :=
    if: try_acquire "l"
    then #()
    else "acquire" "l".
Definition release : val := λ: "l", "l" <- #false.
Definition free : val :=
  rec: "free" "l" := Free "l".

(** The CMRA we need. *)
(* Not bundling heapGS, as it may be shared with other users. *)
Class lockG Σ := LockG { lock_tokG :: inG Σ (authR (optionUR (exclR fracO))) }.
Definition lockΣ : gFunctors := #[GFunctor (authR (optionUR (exclR fracO))) ].

Global Instance subG_lockΣ {Σ} : subG lockΣ Σ → lockG Σ.
Proof. solve_inG. Qed.

Record lock_name := LockName {
  lock_fcinv_name : fcinv_name;
  lock_auth_name : gname;
}.

Definition lock_inv `{!heapGS Σ, !lockG Σ} (γ : lock_name)
    (l : loc) (R : ironProp Σ) : ironProp Σ :=
  (∃ b : bool, l ↦ #b ∗
    if b then ∃ p,
      <affine> ⎡ own (lock_auth_name γ) (● (Excl' p)) ⎤ ∗
      fcinv_own (lock_fcinv_name γ) p
    else
      <affine> ⎡ own (lock_auth_name γ) (● None) ⎤ ∗ R)%I.

Definition is_lock `{!heapGS Σ, !lockG Σ} (N : namespace) (γ : lock_name)
    (lk : val) (R : ironProp Σ) : ironProp Σ :=
  (∃ l : loc, ⌜lk = #l⌝ ∧ fcinv N (lock_fcinv_name γ) (lock_inv γ l R))%I.

Definition unlocked `{!heapGS Σ, !lockG Σ} (γ : lock_name) (p : frac) : ironProp Σ :=
  (fcinv_own (lock_fcinv_name γ) p ∗ fcinv_cancel_own (lock_fcinv_name γ) p)%I.

Definition locked `{!heapGS Σ, !lockG Σ} (γ : lock_name) (p : frac) : ironProp Σ :=
  (fcinv_own (lock_fcinv_name γ) (p/2) ∗  fcinv_cancel_own (lock_fcinv_name γ) p ∗
   <affine> ⎡ own (lock_auth_name γ) (◯ (Excl' (p/2)%Qp)) ⎤)%I.

Section proof.
  Context `{!heapGS Σ, !lockG Σ} (N : namespace).

  Global Instance lock_inv_ne γ l : NonExpansive (lock_inv γ l).
  Proof. solve_proper. Qed.
  Global Instance lock_inv_uniform γ l R : Uniform R → Uniform (lock_inv γ l R).
  Proof. intros. apply exist_uniform=> -[]; apply _. Qed.

  Global Instance is_lock_contractive γ l : Contractive (is_lock N γ l).
  Proof. solve_contractive. Qed.
  Global Instance is_lock_persistent γ lk R : Persistent (is_lock N γ lk R).
  Proof. apply _. Qed.
  Global Instance is_lock_affine γ lk R : Affine (is_lock N γ lk R).
  Proof. apply _. Qed.

  Global Instance unlocked_fractional γ : Fractional (unlocked γ).
  Proof. apply _. Qed.
  Global Instance unlocked_as_fractional γ p :
    AsFractional (unlocked γ p) (unlocked γ) p.
  Proof. split. done. apply _. Qed.
  (* Global Instance unlocked_affine γ p : Affine (unlocked γ p).
  Proof. apply _. Qed. *)
  Global Instance unlocked_timeless γ p : Timeless (unlocked γ p).
  Proof. apply _. Qed.

  (* Global Instance locked_affine γ l : Affine (locked γ l).
  Proof. apply _. Qed. *)
  Global Instance locked_timeless γ l : Timeless (locked γ l).
  Proof. apply _. Qed.
  Lemma locked_exclusive γ p q :
    locked γ p -∗ locked γ q -∗ False.
  Proof.
    iIntros "[_ [_ H1]] [_ [_ H2]]".
    by iCombine "H1 H2" gives %?%auth_frag_op_valid_1.
  Qed.

  (** The main proofs. *)
  Lemma new_lock_spec R :
    {{{ R }}} new_lock #()
    {{{ lk γ, RET lk; is_lock N γ lk R ∗ unlocked γ 1}}}.
  Proof.
    iIntros (Φ) "HR HΦ". rewrite -iron_wp_fupd /new_lock /=.
    wp_lam. wp_alloc l as "Hl".
    iMod (own_alloc (● None)) as (γ) "Hγ"; first by apply auth_auth_valid.
    iMod (fcinv_alloc_named _ N (λ γinv, lock_inv (LockName γinv γ) l R)
      with "[-HΦ]") as (γinv) "(#? & Hγc & Hγ)".
    { iIntros "!>" (γinv). iExists false. by iFrame. }
    iModIntro. iApply ("HΦ" $! #l (LockName γinv γ)).
    iFrame "Hγc". iFrame. iExists l; iFrame; eauto.
  Qed.

  Lemma try_acquire_spec p γ lk R `{!Uniform R} :
    {{{ is_lock N γ lk R ∗ unlocked γ p }}} try_acquire lk
    {{{ b, RET #b; if b is true then locked γ p ∗ R else unlocked γ p }}}.
  Proof.
    iIntros (Φ) "[Hl [Hγinv HC]] HΦ". iDestruct "Hl" as (l ->) "#?". wp_lam.
    wp_bind (CmpXchg _ _ _).
    iMod (fcinv_acc_strong _ N with "[$] [$]") as "(Hinv & Hγinv & Hclose)"; first done.
    iDestruct "Hinv" as ([|]) "[>Hl H]".
    - wp_cmpxchg_fail. iMod ("Hclose" with "[Hl H]").
      { iLeft. iModIntro. iExists true. iFrame. }
      iModIntro. wp_pures. iApply ("HΦ" $! false). iFrame.
    - wp_cmpxchg_suc. iDestruct "H" as "[Hγ HR]". iDestruct "Hγinv" as "[Hγinv Hγinv']".
      iMod (own_update with "Hγ") as "[Hγ Hγf]".
      { by apply auth_update_alloc, (alloc_option_local_update (Excl (p/2)%Qp)). }
      iMod ("Hclose" with "[Hl Hγ Hγinv]").
      { iLeft. iModIntro. iExists true. iFrame "Hl". iExists (p/2)%Qp. iFrame. }
      iModIntro. wp_pures. iApply ("HΦ" $! true with "[-]"). iFrame.
  Qed.

  Lemma acquire_spec p γ lk R `{!Uniform R} :
    {{{ is_lock N γ lk R ∗ unlocked γ p }}} acquire lk
    {{{ RET #(); locked γ p ∗ R }}}.
  Proof.
    iIntros (Φ) "[#? Hunlked] HΦ". iLöb as "IH". wp_rec.
    wp_apply (try_acquire_spec with "[$]"). iIntros ([]).
    - iIntros "[Hlked HR]". wp_if. iApply "HΦ"; iFrame.
    - iIntros "Hlkd". wp_if. by iApply ("IH" with "[$]").
  Qed.

  Lemma release_spec p γ lk R `{!Uniform R} :
    {{{ is_lock N γ lk R ∗ locked γ p ∗ R }}} release lk
    {{{ RET #(); unlocked γ p }}}.
  Proof.
    iIntros (Φ) "(Hlock & [Hγinv [HC Hγf]] & HR) HΦ".
    iDestruct "Hlock" as (l ->) "#?". wp_lam.
    iMod (fcinv_acc _ N with "[$] [$]") as "(Hinv & Hγinv & Hclose)"; first done.
    iDestruct "Hinv" as ([|]) "[Hl Hinv]".
    - wp_store. iDestruct "Hinv" as (p') "[Hγ Hγinv']".
      iCombine "Hγ Hγf"
        gives %[<-%Excl_included%leibniz_equiv _]%auth_both_valid_discrete.
      iMod (own_update_2 with "Hγ Hγf") as "Hγ".
      { eapply auth_update_dealloc, delete_option_local_update; apply _. }
      iMod ("Hclose" with "[HR Hl Hγ]").
      { iNext. iExists false; iFrame. }
      iApply "HΦ". iModIntro. by iFrame.
    - iDestruct "Hinv" as "[>Hγ ?]".
      by iCombine "Hγ Hγf"
        gives %[[[] ?%leibniz_equiv_iff] _]%auth_both_valid_discrete.
  Qed.

  Lemma free_spec γ lk R `{!Uniform R} :
    {{{ is_lock N γ lk R ∗ unlocked γ 1 }}} free lk {{{ RET #(); R }}}.
  Proof.
    iIntros (Φ) "(Hlock & Hunlkd & Hγc) HΦ". iDestruct "Hlock" as (l ->) "#?".
    wp_lam. wp_pures.
    iMod (fcinv_acc_strong _ N with "[$] [$]") as "(Hinv & Hγinv & Hclose)"; first done.
    iDestruct "Hinv" as ([|]) "[>Hl H]".
    - iDestruct "H" as (p) ">[_ Hγinv']".
      by iDestruct (fcinv_own_valid with "Hγinv Hγinv'") as %?%(Qp.not_add_le_l _).
    - wp_free. iDestruct "H" as "[_ HR]".
      iMod ("Hclose" with "[Hγinv Hγc]"); first by (iRight; iFrame).
      by iApply "HΦ".
  Qed.
End proof.

Section adequacy_proofs.
  Context `{HhG : heapGpreS Σ, !lockG Σ} (N : namespace).

  Lemma lock_always_freed_force_location `{!heapGS Σ} l :
    l ↦ #false -∗
    ⎡ ∀ σ m, heap_ctx σ m -∗ ⌜σ !! l = Some #false⌝ ⎤.
  Proof.
    iStartProof (iProp _).
    iIntros (π) "Hl"; iIntros (h Qs) "Hctx".
    iApply (heap_valid with "Hctx Hl ").
  Qed.

  Theorem lock_always_freed (lk : loc) e R σ1 σ2 v es :
    (∀ `{!heapGS Σ},
      Uniform R ∧
      (([∗ map] l ↦ v ∈ omap id σ1.(heap), l ↦ v)
        ={⊤}=∗ ∃ γ, is_lock N γ #lk R ∗ unlocked γ 1) ∧
      ∀ γ, {{{ is_lock N γ #lk R ∗ unlocked γ 1 }}} e {{{ v, RET v; unlocked γ 1 }}}) →
    rtc erased_step ([e], σ1) (of_val v :: es, σ2) →
    σ2.(heap) !! lk = Some (Some #false).
  Proof using HhG.
    intros Hpre.
    apply (heap_adequacy Σ NotStuck e σ1 (λ v σ, σ.(heap) !! lk = Some (Some #false))).
    iIntros (?) "Hlocs".
    destruct (Hpre _) as (Huniform & Hinit & Hspec); clear Hpre.
    iMod (Hinit with "Hlocs") as (γ) "[#Hlock Hunlocked]"; clear Hinit.
    iApply (Hspec with "[$Hunlocked //]").
    iIntros "!>" (_) "[Hown Hcancel]".
    iDestruct "Hlock" as (l) "[% #Hinv]"; simplify_eq.
    iMod (fcinv_acc_strong with "Hinv Hown") as "(Hlock_inv & Hown & ?)"; first done.
    iDestruct "Hlock_inv" as ([]) "[>Hl H]".
    - iDestruct "H" as (p) "[? >Hown']".
      by iDestruct (fcinv_own_valid with "Hown Hown'") as %Hcontra%Qp.not_add_le_l.
    - iMod (fupd_mask_subseteq ∅); first solve_ndisj.
      iIntros "!>" (σ' m) "Hh". rewrite -lookup_omap_id_Some.
      by iApply (lock_always_freed_force_location with "Hl").
  Qed.
End adequacy_proofs.

Global Typeclasses Opaque is_lock locked unlocked.
