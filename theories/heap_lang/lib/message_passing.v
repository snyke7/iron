(* message_passing.v: This file contains the verification of the main example in the paper:
 * the channel module. This proof proceeds entirely within the lifted logic. The main
 * proofs are found in send_{x, y}_spec, receive_{x, y}_spec, close_{x, y}_spec,
 * and new_channel_spec. These specify the operations for sending, receiving, closing,
 * and initializing respectively.
 *)
From iris.heap_lang Require Export notation.
From iron.iron_logic Require Import fcinv.
From iris.algebra Require Import agree frac excl csum.
From iron.heap_lang Require Export lib.queue.
From iron.heap_lang Require Import proofmode.
Set Default Proof Using "Type".

Definition cleanup : val :=
  rec: "cleanup" "d" "q_x" "q_y" "x_alive" "y_alive" :=
    if: !"x_alive" then
      "cleanup" "d" "q_x" "q_y" "x_alive" "y_alive"
    else if: !"y_alive" then
      "cleanup" "d" "q_x" "q_y" "x_alive" "y_alive"
    else
      delete_all "d" "q_x";;
      delete_all "d" "q_y";;
      Free "x_alive";;
      Free "y_alive".

Definition new_channel : val := λ: "d",
  let: "q_x" := new_queue #() in
  let: "q_y" := new_queue #() in
  let: "x_alive" := ref #true in
  let: "y_alive" := ref #true in
  Fork (cleanup "d" "q_x" "q_y" "x_alive" "y_alive");;
  (* First component is the receive queue, the second the send queue. *)
  (("q_x", "q_y", "x_alive"),
   ("q_y", "q_x", "y_alive")).

Definition send : val := λ: "p" "x",
  let: "send_q" := Snd (Fst "p") in
  enqueue "send_q" "x".

Definition receive : val := λ: "p",
  let: "receive_q" := Fst (Fst "p") in
  let: "receive" := rec: "rcv" <> :=
    match: dequeue "receive_q" with
      NONE => "rcv" #()
    | SOME "x" => "x"
    end
  in "receive" #().

Definition close : val := λ: "p",
  let: "flag" := Snd "p" in "flag" <- #false.

Definition binary_choiceR := csumR (exclR unitO) (agreeR boolO).
Definition no_choice : binary_choiceR := Cinl (Excl ()).
Definition chose (n : bool) : binary_choiceR := Cinr (to_agree n).

Class binary_choiceG Σ := { binary_choice_inG :: inG Σ binary_choiceR }.
Definition binary_choiceΣ : gFunctors := #[GFunctor binary_choiceR].
Global Instance subG_binary_choiceΣ {Σ} : subG binary_choiceΣ Σ → binary_choiceG Σ.
Proof. solve_inG. Qed.

Section proofs.
  Context (N : namespace) `{heapGS Σ, queueG Σ, binary_choiceG Σ}.
  Context (Ψ : val → ironProp Σ) `{!∀ x, Uniform (Ψ x)}.

  Record message_names :=
  GNames { x_alive_tok : gname;
           y_alive_tok : gname;
           x_or_y_commitment : gname;
           cleanup_tok : gname; (* Cleanup tokens used by the cleanup thread to
                                   mark it is done. *)
           γsx : queue_bag_name; (* Ghost names for each of the bags. *)
           γsy : queue_bag_name;
           γinv : fcinv_name;
         }.

  Definition messaging_sts
      (g : message_names) (x_alive y_alive : loc) (px py : val) : ironProp Σ :=
    (* The message queues. *)
    ((
      (* Both of the endpoints are alive. *)
      x_alive ↦ #true ∗ y_alive ↦ #true
      ∗ <affine> ⎡ own (x_or_y_commitment g) no_choice ⎤
    ) ∨ (
      (* The x endpoint is closed (and y open). *)
      x_alive ↦ #false ∗ y_alive ↦ #true
      ∗ <affine> ⎡ own (x_or_y_commitment g) (chose false) ⎤
      ∗ <affine> ⎡ own (x_alive_tok g) (Excl ()) ⎤
      ∗ fcinv_own (γinv g) (1 / 2 / 2)%Qp
      ∗ insert_handle (γsy g) py
      ∗ remove_handle (γsx g) px
    ) ∨ (
      (* The y endpoint only is closed (and x open). *)
      x_alive ↦ #true ∗ y_alive ↦ #false
      ∗ <affine> ⎡ own (x_or_y_commitment g) (chose true) ⎤
      ∗ <affine> ⎡ own (y_alive_tok g) (Excl ()) ⎤
      ∗ fcinv_own (γinv g) (1 / 2 / 2)%Qp
      ∗ insert_handle (γsx g) px
      ∗ remove_handle (γsy g) py
    ) ∨ (
      (* Both endpoints are closed. *)
      x_alive ↦ #false ∗ y_alive ↦ #false
      ∗ <affine> ⎡ own (x_alive_tok g) (Excl ()) ⎤
      ∗ <affine> ⎡ own (y_alive_tok g) (Excl ()) ⎤
      ∗ fcinv_own (γinv g) (1 / 2)%Qp
      ∗ insert_handle (γsy g) py
      ∗ insert_handle (γsx g) px
      ∗ remove_handle (γsx g) px
      ∗ remove_handle (γsy g) py
    ))%I.

  Global Instance messaging_sts_hasperm g x_alive y_alive px py :
    Uniform (messaging_sts g x_alive y_alive px py).
  Proof. apply _. Qed.

  Definition message_inv
      (px py : val) (g : message_names)
      (x_alive y_alive : loc) : ironProp Σ :=
    (fcinv (N .@ "sts") (γinv g) (messaging_sts g x_alive y_alive px py)
     ∗ <affine> is_queue_bag (N .@ "xbag") (γsx g) px Ψ
     ∗ <affine> is_queue_bag (N .@ "ybag") (γsy g) py Ψ)%I.

  Definition endpoint_x (γ : message_names) (p : val) : ironProp Σ :=
    (∃ (q_en q_de : val) (x_alive y_alive : loc),
       <affine> ⌜p = (q_de, q_en, #x_alive)%V⌝
     ∗ <affine> ⎡ own (x_alive_tok γ) (Excl ()) ⎤
     ∗ remove_handle (γsx γ) q_de
     ∗ insert_handle (γsy γ) q_en
     ∗ fcinv_own (γinv γ) (1/2/2)
     ∗ message_inv q_de q_en γ x_alive y_alive)%I.

  Definition endpoint_y (γ : message_names) (p : val) : ironProp Σ :=
    (∃ (q_en q_de : val) (x_alive y_alive : loc),
       <affine> ⌜p = (q_de, q_en, #y_alive)%V⌝
     ∗ <affine> ⎡ own (y_alive_tok γ) (Excl ()) ⎤
     ∗ remove_handle (γsy γ) q_de
     ∗ insert_handle (γsx γ) q_en
     ∗ fcinv_own (γinv γ) (1/2/2)
     ∗ message_inv q_en q_de γ x_alive y_alive)%I.

  Definition endpoint (b : bool) : message_names → val → ironProp Σ :=
    if b then endpoint_x else endpoint_y.

  Lemma cleanup_spec (d : val) g x_alive y_alive px py :
    {{{
        (∀ u, {{{ Ψ u }}} d u {{{ v, RET v; emp }}})
      ∗ message_inv px py g x_alive y_alive
      ∗ fcinv_own (γinv g) (1/2)
      ∗ fcinv_cancel_own (γinv g) 1
      ∗ queue_bag_cancel_own (γsx g) 1
      ∗ queue_bag_cancel_own (γsy g) 1
      ∗ <affine> ⎡ own (cleanup_tok g) (Excl ()) ⎤
    }}}
      cleanup d px py #x_alive #y_alive
    {{{ RET #(); emp }}}.
  Proof using H2.
    iIntros (Φ) "(#Hdestruct & [#Hinv [Hbag1 Hbag2]] &
      Hinv_own & Hinv_cancel & Hcancel1 & Hcancel2 & Hown) HΦ".
    iLöb as "IH". wp_rec. repeat wp_let.
    wp_bind (! _)%E.
    iMod (fcinv_acc_strong _ (N .@ "sts") with "Hinv Hinv_own")
      as "(Hinv' & Hinv_own & Hclose)"; auto.
    iDestruct "Hinv'" as "[H1 | [H2 | [H3 | H4]]]".
    - iDestruct "H1" as "[>Hx Hrest]".
      wp_load.
      iMod ("Hclose" with "[Hx Hrest]") as "_".
      { iLeft; iNext; iLeft; iFrame. }
      iModIntro.
      wp_if.
      wp_apply ("IH" with "Hbag1 Hbag2 Hinv_own Hinv_cancel Hcancel1 Hcancel2 Hown HΦ").
    - iDestruct "H2" as "(>Hx & >Hy & >#Hchoice & Hrest)".
      wp_load.
      iMod ("Hclose" with "[Hx Hy Hrest]") as "_".
      { iLeft; iNext; iRight; iLeft; iFrame; by iModIntro. }
      iModIntro.
      wp_if.

      wp_bind (Load _).
      iMod (fcinv_acc_strong _ (N .@ "sts") with "Hinv Hinv_own")
        as "(Hinv' & Hinv_own & Hclose)"; auto.
      iDestruct "Hinv'" as "[H1 | [H2 | [H3 | H4]]]".
      + iDestruct "H1" as "(>Hx & >Hy & Hrest)".
        wp_load.
        iMod ("Hclose" with "[Hx Hy Hrest]") as "_".
        { iLeft. iNext; iLeft; iFrame. }
        iModIntro.
        wp_if.
        wp_apply ("IH" with "Hbag1 Hbag2 Hinv_own Hinv_cancel Hcancel1 Hcancel2 Hown HΦ").
      + iDestruct "H2" as "[Hx [>Hy Hrest]]".
        wp_load.
        iMod ("Hclose" with "[Hx Hy Hrest]") as "_".
        { iLeft; iNext; iRight; iLeft; iFrame. }
        iModIntro.
        wp_if.
        wp_apply ("IH" with "Hbag1 Hbag2 Hinv_own Hinv_cancel Hcancel1 Hcancel2 Hown HΦ").
      + iDestruct "H3" as "[? [? [>#Hchoice' ?]]]".
        iCombine "Hchoice Hchoice'"
          gives %[=]%cmra_discrete_valid%to_agree_op_inv_L.
      + iDestruct "H4" as "(Hx & Hy & Hownx & Howny & Hinv_own' &
          Hinserty & Hinsertx & Hremovex & Hremovey)".
        wp_load.
        iMod ("Hclose" with "[Hinv_cancel Hinv_own Hinv_own']") as "_".
        { iRight; by iFrame "∗". }
        iModIntro.
        wp_if.
        wp_apply (bag_delete_all_spec _ Ψ
          with "[] [Hbag1 Hinsertx Hremovex Hcancel1]"); first done.
        * iIntros (v) "!#". iIntros (Φ') "HΨ' HΦ'".
          wp_apply ("Hdestruct" with "HΨ'"); auto.
        * iFrame.
        * iIntros "_".
          wp_apply (bag_delete_all_spec _ Ψ
            with "[] [Hbag2 Hinserty Hremovey Hcancel2]"); first done.
          ** iIntros (v) "!#"; iIntros (Φ') "HΨ' HΦ'".
             wp_apply ("Hdestruct" with "HΨ'"); auto.
          ** iFrame.
          ** iIntros "_". do 2 wp_free. by iApply "HΦ".
    - iDestruct "H3" as "[>Hx Hrest]".
      wp_load.
      iMod ("Hclose" with "[Hx Hrest]") as "_".
      { iLeft; iNext; iRight; iRight; iLeft; iFrame. }
      iModIntro.
      wp_if.
      wp_apply ("IH" with "Hbag1 Hbag2 Hinv_own Hinv_cancel Hcancel1 Hcancel2 Hown HΦ").
    - iDestruct "H4" as "(Hx & Hy & Hownx & Howny & Hinv_own' &
        Hinserty & Hinsertx & Hremovex & Hremovey)".
      wp_load.
      iMod ("Hclose" with "[Hinv_own Hinv_own' Hinv_cancel]") as "_".
      { iRight; by iFrame "∗". }
      iModIntro.
      wp_if.
      wp_load.
      wp_if.
      wp_apply (bag_delete_all_spec _ Ψ
        with "[] [Hbag1 Hinsertx Hremovex Hcancel1]"); first done.
      * iIntros (v) "!#". iIntros (Φ') "HΨ' HΦ'".
        wp_apply ("Hdestruct" with "HΨ'"); auto.
      * iFrame.
      * iIntros "_".
        wp_apply (bag_delete_all_spec _ Ψ
          with "[] [Hbag2 Hinserty Hremovey Hcancel2]"); first done.
        ** iIntros (v) "!#". iIntros (Φ') "HΨ' HΦ'".
           wp_apply ("Hdestruct" with "HΨ'"); auto.
        ** iFrame.
        ** iIntros "_". do 2 wp_free. by iApply "HΦ".
  Qed.

  Theorem new_channel_spec (d : val) :
    {{{ (∀ u, {{{ Ψ u }}} d u {{{ v, RET v; emp }}}) }}}
      new_channel d
    {{{ (px py : val) (γ : message_names), RET (px, py);
      endpoint_x γ px ∗ endpoint_y γ py
    }}}.
  Proof using H2.
    iIntros (Φ) "#Hdestruct HΦ". wp_lam.
    wp_apply (new_queue_bag_spec (N .@ "xbag") Ψ); auto.
    iIntros (γxs px) "(#Hbag1 & Hcancel1 & Hinsertx & Hremovex)".
    wp_let.
    wp_apply (new_queue_bag_spec (N .@ "ybag") Ψ); auto.
    iIntros (γys py) "(#Hbag2 & Hcancel2 & Hinserty & Hremovey)".
    wp_let.
    wp_alloc x_alive as "Hx". wp_let.
    wp_alloc y_alive as "Hy". wp_let.
    iMod (own_alloc (Excl ())) as (γclean) "Hclean"; first done.
    iMod (own_alloc (Excl ())) as (γx_alive) "Hx_alive"; first done.
    iMod (own_alloc (Excl ())) as (γy_alive) "Hy_alive"; first done.
    iMod (own_alloc no_choice) as (x_or_y_commitment) "Hnochoice"; first done.
    iMod (fcinv_alloc_strong _ (N .@ "sts"))
      as (γinv) "([Hinv_own1 [Hinv_own2 Hinv_own3]] & Hcreate)".
    set γs := GNames γx_alive γy_alive x_or_y_commitment γclean γxs γys γinv.
    iMod ("Hcreate" $! (messaging_sts γs x_alive y_alive px py)
      with "[Hx Hy Hnochoice]") as "[#Hinv Hinv_cancel]".
    { iNext. iLeft; iFrame. }
    wp_apply (iron_wp_fork
      with "[-Hcancel1 Hcancel2 Hclean Hinv_cancel Hinv_own1]"); last first.
    { wp_apply (cleanup_spec d γs
        with "[Hcancel1 Hcancel2 Hclean Hinv_cancel Hinv_own1]"); last auto.
      rewrite /message_inv; iFrame; eauto. }
    iIntros "!>". wp_pures.
    iApply ("HΦ" $! _ _ γs).
    iSplitL "Hinserty Hremovex Hx_alive Hinv_own2";
      iExists _, _, _, _; by iFrame "∗#".
  Qed.

  Lemma send_x_spec γ p x :
    {{{ endpoint_x γ p ∗ Ψ x }}} send p x {{{ RET #(); endpoint_x γ p }}}.
  Proof using H2.
    iIntros (Φ) "[Hend HΨ] HΦ". wp_lam.
    iDestruct "Hend" as (q_en q_de x_alive y_alive ->)
      "(Hxalive & Hremove & Hinsert & Hinv_own & #(Hinv & Hbag1 & Hbag2))".
    wp_apply (bag_insert_spec (N .@ "ybag") Ψ with "[$Hbag2 $Hinsert $HΨ]"); first done.
    iIntros "Hinsert".
    iApply "HΦ". iExists _, _, _, _. iFrame. repeat iSplit; eauto.
  Qed.

  Lemma send_y_spec γ p x :
    {{{ endpoint_y γ p ∗ Ψ x }}} send p x {{{ RET #(); endpoint_y γ p }}}.
  Proof using H2.
    iIntros (Φ) "[Hend HΨ] HΦ". wp_lam.
    iDestruct "Hend" as (q_en q_de x_alive y_alive ->)
      "(Hyalive & Hremove & Hinsert & Hinv_own & #(Hinv & #Hbag1 & #Hbag2))".
    wp_apply (bag_insert_spec _ Ψ with "[$Hbag1 $Hinsert $HΨ]"); first done.
    iIntros "Hinsert".
    iApply "HΦ". iExists _, _, _, _. iFrame. repeat iSplit; eauto.
  Qed.

  Lemma close_x_spec γ p :
    {{{ endpoint_x γ p }}} close p {{{ RET #(); emp }}}.
  Proof.
    iIntros (Φ) "Hend HΦ".
    iDestruct "Hend" as (q_en q_de x_alive y_alive ->)
      "(Hxalive & Hremove & Hinsert & Hinv_own & #(Hinv & Hbag1 & Hbag2))".
    wp_lam. wp_proj. wp_let.
    iMod (fcinv_acc_strong _ (N .@ "sts") with "Hinv Hinv_own")
      as "(Hinv' & Hinv_own & Hclose)"; auto.
    iDestruct "Hinv'" as "[H1 | [H2 | [H3 | H4 ]]]".
    - iDestruct "H1" as "(Hx & Hy & Hchoice)".
      wp_store.
      iMod (own_update _ _ (chose false) with "Hchoice") as "Hchoice".
      { by apply cmra_update_exclusive. }
      iMod ("Hclose" with "[Hchoice Hxalive Hinv_own Hx Hy Hinsert Hremove]").
      { iLeft; iNext; iRight; iLeft. iFrame. }
      iModIntro; by iApply "HΦ".
    - iDestruct "H2" as "(? & ? & ? & >Hxalive' & ?)".
      iCombine "Hxalive Hxalive'" gives %[].
    - iDestruct "H3" as "(Hx & Hy & Hchoice & Hyalive & Hrest)".
      wp_store.
      iMod ("Hclose"
        with "[Hchoice Hxalive Hyalive Hrest Hinv_own Hx Hy Hinsert Hremove]").
      { iLeft; iNext; iRight; iRight; iRight; iFrame. }
      iModIntro; by iApply "HΦ".
    - iDestruct "H4" as "(? & ? & >Hxalive' & ?)".
      iCombine "Hxalive Hxalive'" gives %[].
  Qed.

  Lemma close_y_spec γ p : {{{ endpoint_y γ p }}} close p {{{ RET #(); emp }}}.
  Proof.
    iIntros (Φ) "Hend HΦ".
    iDestruct "Hend" as (q_en q_de x_alive y_alive ->)
      "(Hyalive & Hremove & Hinsert & Hinv_own & #(Hinv & Hbag1 & Hbag2))".
    wp_lam. wp_proj. wp_let.
    iMod (fcinv_acc_strong _ (N .@ "sts") with "Hinv Hinv_own")
      as "[Hinv' [Hinv_own  Hclose]]"; auto.
    iDestruct "Hinv'" as "[H1 | [H2 | [H3 | H4]]]".
    - iDestruct "H1" as "(Hx & Hy & Hchoice)".
      wp_store.
      iMod (own_update _ _ (chose true) with "Hchoice") as "Hchoice".
      { by apply cmra_update_exclusive. }
      iMod ("Hclose" with "[Hchoice Hyalive Hx Hy Hinv_own Hinsert Hremove]").
      { iLeft; iNext; iRight; iRight; iLeft; iFrame. }
      iModIntro; by iApply "HΦ".
    - iDestruct "H2" as "(Hx & Hy & Hchoice & Hxalive & Hrest)".
      wp_store.
      iMod ("Hclose"
        with "[Hchoice Hxalive Hyalive Hrest Hx Hy Hinv_own Hinsert Hremove]").
      { iLeft; iNext; iRight; iRight; iRight; iFrame. }
      iModIntro; by iApply "HΦ".
    - iDestruct "H3" as "(? & ? & ? & >Hyalive' & ?)".
      iCombine "Hyalive Hyalive'" gives %[].
    - iDestruct "H4" as "(? & ? & ? & >Hyalive' & ?)".
      iCombine "Hyalive Hyalive'" gives %[].
  Qed.

  Lemma receive_x_spec γ p :
    {{{ endpoint_x γ p }}} receive p {{{ x, RET x; endpoint_x γ p ∗ Ψ x }}}.
  Proof using H2.
    iIntros (Φ) "Hend HΦ". wp_lam.
    iDestruct "Hend" as (q_en q_de x_alive y_alive ->)
      "(Hxalive & Hremove & Hinsert & Hinv_own & #(Hinv & Hbag1 & Hbag2))".
    do 7 wp_pure _. iLöb as "IH". wp_rec.
    wp_apply (bag_remove_spec _ Ψ with "[$Hbag1 $Hremove]"); first done.
    iIntros (v). iDestruct 1 as "[[-> Hremove] | H]".
    - wp_match.
      iApply ("IH" with "Hxalive Hremove Hinsert Hinv_own HΦ").
    - iDestruct "H" as (u ->) "[Hremove HΨ']".
      wp_match.
      iApply "HΦ". iFrame. iExists _, _, _, _. iFrame. repeat iSplit; eauto.
  Qed.

  Lemma receive_y_spec γ p :
    {{{ endpoint_y γ p }}} receive p {{{ x, RET x; endpoint_y γ p ∗ Ψ x }}}.
  Proof using H2.
    iIntros (Φ) "Hend HΦ". wp_lam.
    iDestruct "Hend" as (q_en q_de x_alive y_alive ->)
      "(Hyalive & Hremove & Hinsert & Hinv_own & #(Hinv & Hbag1 & Hbag2))".
    do 7 wp_pure _. iLöb as "IH". wp_rec.
    wp_apply (bag_remove_spec _ Ψ with "[$Hbag2 $Hremove]"); first done.
    iIntros (v). iDestruct 1 as "[[-> Hremove] | H]".
    - wp_match.
      iApply ("IH" with "Hyalive Hremove Hinsert Hinv_own HΦ").
    - iDestruct "H" as (u ->) "[Hremove HΨ']".
      wp_match.
      iApply "HΦ". iFrame. iExists _, _, _, _. iFrame. repeat iSplit; eauto.
  Qed.
End proofs.
