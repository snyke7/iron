(* queue.v: In this file we have verified the particular queue used in the paper for
 * implementing the channel module.
 *
 * For the channel module itself it only matters that the queue behaves as a bag does:
 * we wish to enforce that every member of the queue will satisfy some predicate. This
 * specification is rather weak in some settings however, and so we derive it in terms
 * of a more precise HOCAP specification. The HOCAP specification is not given in
 * the paper but it is proven in the [queue_spec] section. From this more precise
 * specification we may derive the bag specifications given in the paper and we
 * have done so in the [queue_bag_spec] section. The specifications given in the paper
 * for insertion, removal, disposal, and so on may be found in this section.
 *)
From iris.heap_lang Require Export notation.
From iron.heap_lang Require Import proofmode.
From iron.iron_logic Require Import fcinv.
From iris.algebra Require Import auth agree excl list.
Set Default Proof Using "Type".

Definition new_queue : val := λ: <>,
  let: "node" := ref NONE in
  (ref "node", ref "node").

(* A better way to do it would be for the head and tail pointers to just point
   to the pointer in the node (its second component). We leave this modification
   and its verification as an exercise to the reader :). *)
Definition enqueue : val := λ: "q" "x",
  let: "tptr" := Snd "q" in
  let: "t" := !"tptr"  in
  let: "newt" := ref NONE in
  (* The following store is not actually atomic. In a C like language, it should
  be implemented by first setting the fields, and finally changing the tag from
  `NONE` into `SOME` in an atomic step. *)
  "t" <- SOME ("x", "newt");;
  "tptr" <- "newt".

Definition dequeue : val := λ: "q",
  let: "hptr" := Fst "q" in
  let: "h" := !"hptr" in
  match: !"h" with
    NONE => NONE (* Empty queue. *)
  | SOME "node" =>
     let: "newh" := Snd "node" in
     "hptr" <- "newh";;
     Free "h";;
     SOME (Fst "node")
  end.

(* Only works correctly when the queue is empty. Otherwise it will leak memory. *)
Definition delete : val := λ: "q",
  let: "hptr" := Fst "q" in
  let: "tptr" := Snd "q" in
  let: "h" := !"hptr" in
  Free "h";; Free "hptr";; Free "tptr".

(* Parametrized by a "destructor". See the bag_dealloc below for example
specification. *)
Definition delete_all : val :=
  rec: "go" "f" "q" :=
    let: "r" := dequeue "q" in
    match: "r" with
      NONE => delete "q"
    | SOME "x" => "f" "x";; "go" "f" "q"
    end.

Class queueG Σ := {
  queueG_inG :: inG Σ (prodR fracR (agreeR (listO valO)));
  queueG_dequeue_inG :: inG Σ (exclR unitO);
  queueG_enqueue_inG :: inG Σ (authR (optionUR (exclR locO)))
}.

Record queue_name := QueueName {
  queue_fcinv_name : fcinv_name;
  queue_own_name : gname;
  queue_dequeue_name : gname;
  queue_enqueue_name : gname;
}.

(*
Definition queueΣ : gFunctors := #[GFunctor queueCmra; GFunctor fracR;
  GFunctor (exclR unitO); fcinvΣ].

Instance subG_queueΣ {Σ} : subG queueΣ Σ → queueG Σ.
Proof. solve_inG. Qed.
*)

Definition queue_own_def `{queueG Σ} (γ : queue_name)
    (q : Qp) (xs : list val) : ironProp Σ :=
  (<affine> ⎡ own (queue_own_name γ) (q, to_agree xs) ⎤)%I.
Definition queue_own_aux `{queueG Σ} : seal (queue_own_def). by eexists. Qed.
Definition queue_own `{queueG Σ} := unseal queue_own_aux.
Definition queue_own_eq `{queueG Σ} : queue_own = _ := seal_eq queue_own_aux.

Notation "γ ⤇{ q } xs" := (queue_own γ q xs)
  (at level 20, q at level 50, format "γ  ⤇{ q }  xs") : bi_scope.

Fixpoint is_queue_list `{heapGS Σ} (lh lt : loc) (vs : list val) : ironProp Σ :=
  match vs with
  | [] => <affine> ⌜lh = lt⌝
  | v :: vs => ∃ l : loc, lh ↦ SOMEV (v, #l) ∗ is_queue_list l lt vs
  end%I.

Definition queue_inv `{heapGS Σ, queueG Σ} (γ : queue_name)
    (lhptr ltptr : loc) : ironProp Σ :=
  (∃ (lh lt : loc) (vs : list val),
    γ ⤇{1/2} vs ∗
    lhptr ↦{1/2} #lh ∗
    <affine> ⎡ own (queue_enqueue_name γ) (● (Excl' lt)) ⎤ ∗
    match vs with
    | [] => <affine> ⌜ lh = lt ⌝
    | v :: vs => ∃ l : loc,
       (lh ↦{1} SOMEV (v, #l)
        ∨ <affine> ⎡ own (queue_dequeue_name γ) (Excl ()) ⎤ ∗
        lh ↦{1/2} SOMEV (v, #l)) ∗
       is_queue_list l lt vs
    end ∗
    lt ↦ NONEV)%I.

Definition is_queue `{heapGS Σ, queueG Σ} (N : namespace)
    (γ : queue_name) (q : val) : ironProp Σ :=
  (∃ lhptr ltptr : loc,
    ⌜q = (#lhptr, #ltptr)%V⌝ ∧
    fcinv N (queue_fcinv_name γ) (queue_inv γ lhptr ltptr))%I.

Definition queue_cancel_own `{heapGS Σ, queueG Σ}
    (γ : queue_name) (p : frac) : ironProp Σ :=
  fcinv_cancel_own (queue_fcinv_name γ) p.

Definition enqueue_handle `{heapGS Σ, queueG Σ}
    (γ : queue_name) (q : val) : ironProp Σ :=
  (∃ lhptr ltptr lt : loc,
    ⌜q = (#lhptr, #ltptr)%V⌝ ∧
    fcinv_own (queue_fcinv_name γ) (1/2) ∗
    <affine> ⎡ own (queue_enqueue_name γ) (◯ (Excl' lt)) ⎤ ∗
    ltptr ↦ #lt)%I.

Definition dequeue_handle `{heapGS Σ, queueG Σ}
    (γ: queue_name) (q : val) : ironProp Σ :=
  (∃ (lhptr ltptr : loc) v,
    ⌜q = (#lhptr, #ltptr)%V⌝ ∧
    fcinv_own (queue_fcinv_name γ) (1/2) ∗
    lhptr ↦{1/2} v ∗
    <affine> ⎡ own (queue_dequeue_name γ) (Excl ()) ⎤)%I.

Section queue_own.
  Context `{!queueG Σ}.

  Global Instance queue_own_affine γ q vs : Affine (queue_own γ q vs).
  Proof. rewrite queue_own_eq. apply _. Qed.
  Global Instance queue_own_uniform `{heapGS Σ} γ q vs : Uniform (queue_own γ q vs).
  Proof. apply affine_uniform, _. Qed.

  Global Instance queue_own_fractional γ vs : Fractional (λ q, queue_own γ q vs).
  Proof.
    rewrite queue_own_eq /queue_own_def=> p q; iSplit.
    - iIntros "[$$]".
    - iIntros "[H1 H2]". by iSplitL "H1".
  Qed.
  Global Instance makeElem_as_fractional γ vs q :
    AsFractional (γ ⤇{q} vs) (λ q, queue_own γ q vs) q.
  Proof. split. done. apply _. Qed.

  Global Instance queue_own_timeless γ p vs : Timeless (γ ⤇{p} vs).
  Proof. rewrite queue_own_eq. apply fracPred_at_timeless_alt. apply _. Qed.

  Lemma queue_own_agree γ p q vs ws : γ ⤇{p} vs -∗ γ ⤇{q} ws -∗ ⌜vs = ws⌝.
  Proof.
    rewrite queue_own_eq /queue_own_def; iIntros "H1 H2".
    by iCombine "H1 H2" gives %[_ ?%to_agree_op_inv_L].
  Qed.
  Lemma queue_own_update γ vs vs' : γ ⤇{1} vs ==∗ γ ⤇{1} vs'.
  Proof.
    rewrite queue_own_eq /queue_own_def. iIntros "H".
    iMod (own_update with "H") as "$"; last by auto.
    by apply cmra_update_exclusive.
  Qed.
  Lemma queue_own_update_2 γ vs vs' vs'' :
    γ ⤇{1/2} vs -∗ γ ⤇{1/2} vs' ==∗ γ ⤇{1} vs''.
  Proof.
    iIntros "H1 H2". iDestruct (queue_own_agree with "H1 H2") as %->.
    iCombine "H1 H2" as "H". by iApply queue_own_update.
  Qed.
End queue_own.

Section queue_spec.
  Context `{!heapGS Σ, !queueG Σ} (N : namespace).
  Implicit Types E : coPset.
  Implicit Types P Q : ironProp Σ.

  Instance is_queue_list_uniform lh lt vs : Uniform (is_queue_list lh lt vs).
  Proof. revert lh. induction vs; apply _. Qed.
  Instance queue_inv_uniform γ lhptr ltptr :
    Uniform (queue_inv γ lhptr ltptr).
  Proof. repeat (apply exist_uniform=> ?); case_match; apply _. Qed.

  Global Instance is_queue_persistent γ q : Persistent (is_queue N γ q).
  Proof. apply _. Qed.
  Global Instance is_queue_affine γ q : Affine (is_queue N γ q).
  Proof. apply _. Qed.
  Global Instance is_queue_uniform γ q : Uniform (is_queue N γ q).
  Proof. apply affine_uniform, _. Qed.

  Global Instance enqueue_handle_uniform γ q : Uniform (enqueue_handle γ q).
  Proof. apply _. Qed.
  Global Instance dequeue_handle_uniform γ q : Uniform (dequeue_handle γ q).
  Proof. apply _. Qed.

  Lemma new_queue_spec E :
    ↑N ⊆ E →
    {{{ emp }}}
      new_queue #() @ E
    {{{ q γ, RET q; <affine> is_queue N γ q ∗ queue_cancel_own γ 1 ∗
          enqueue_handle γ q ∗ dequeue_handle γ q ∗ γ ⤇{1/2} [] }}}.
  Proof.
    iIntros (? Φ) "_ HΦ". iApply iron_wp_fupd. wp_lam. wp_alloc l as "Hl"; wp_let.
    wp_alloc ltptr as "Hltptr". wp_alloc lhptr as "Hlhptr". 
    iDestruct "Hlhptr" as "[Hlhptr Hlhptr']".
    iMod (own_alloc (1%Qp, to_agree [])) as (γ) "[Hγ Hγ']"; first done.
    iMod (own_alloc (Excl ())) as (γd) "Hγd"; first done.
    iMod (own_alloc (● (Excl' l) ⋅ ◯ (Excl' l))) as (γe) "[Hγe Hγe']";
      first by apply auth_both_valid_discrete.
    iMod (fcinv_alloc_named _ N (λ γinv, queue_inv (QueueName γinv γ γd γe) lhptr ltptr)
      with "[Hlhptr' Hγ' Hl Hγe]") as (γinv) "(#? & Hγc & [Hγinv Hγinv'])".
    { iIntros "!>" (γinv).
      iExists l, l, []. rewrite queue_own_eq /=. iFrame; auto. }
    wp_pures. iModIntro. iApply ("HΦ" $! _ (QueueName γinv γ γd γe)). iSplitR.
    { iExists lhptr, ltptr; auto. }
    iFrame "Hγc". iSplitL "Hγinv Hltptr Hγe'".
    { iExists lhptr, ltptr, l. iFrame; auto. }
    iSplitL "Hγinv' Hlhptr Hγd".
    { iExists lhptr, ltptr. iFrame; auto. }
    by rewrite queue_own_eq.
  Qed.

  Lemma dequeue_spec E Φ q γ :
    ↑N ⊆ E →
    is_queue N γ q -∗ dequeue_handle γ q -∗
    (∀ vs, γ ⤇{1/2} vs ={E∖↑N}=∗
       match vs with
       | [] => γ ⤇{1/2} [] ∗ ▷ (dequeue_handle γ q -∗ Φ NONEV)
       | v :: vs' => γ ⤇{1/2} vs' ∗ ▷ (dequeue_handle γ q -∗ Φ (SOMEV v))
       end) -∗
    WP dequeue q @ E {{ Φ }}.
  Proof.
    iIntros (?) "Hq Hh Hupd".
    iDestruct "Hq" as (lhptr ltptr ->) "#?".
    iDestruct "Hh" as (?? l ?%eq_sym) "(Hγinv & Hlhptr & Hγd)"; simplify_eq.
    wp_lam. wp_load. wp_let. wp_bind (! _)%E.
    iMod (fcinv_acc _ N with "[$] Hγinv") as "(H & Hγinv & Hclose)"; first done.
    iDestruct "H" as (lh lt vs) "(>Hvs & >Hlhptr' & >Hγe & Hlist & >Hlt)".
    iDestruct (pointsto_agree with "Hlhptr Hlhptr'") as %->; simpl.
    destruct vs as [|v vs'].
    { iDestruct "Hlist" as ">->".
      wp_load. iMod ("Hupd" with "Hvs") as "[Hvs HΦ]".
      iMod ("Hclose" with "[Hvs Hlhptr' Hlt Hγe]") as "_".
      { iExists lt, lt, []. iFrame; auto. }
      iModIntro. wp_pures. iApply "HΦ". iExists lhptr, ltptr. eauto with iFrame. }
    iDestruct "Hlist" as (l') "[>[[Hlh Hlh']|[Hγd' ?]] Hlist]"; last first.
    { iCombine "Hγd Hγd'" gives %[]. }
    wp_load.
    iMod ("Hclose" with "[Hvs Hlhptr' Hlh' Hγd Hlt Hlist Hγe]") as "_".
    { iExists lh, lt, (v :: vs'). iNext. iFrame "Hvs Hlhptr' Hlt Hγe".
      iExists l'. auto with iFrame. }
    clear vs'. iModIntro. wp_match. wp_proj. wp_let. wp_bind (_ <- _)%E.
    iMod (fcinv_acc _ N with "[$] Hγinv") as "(H & Hγinv & Hclose)"; first done.
    iDestruct "H" as (lh' lt' vs) "(>Hvs & >Hlhptr' & >Hγe & Hlist & >Hlt)".
    iDestruct (pointsto_agree with "Hlhptr Hlhptr'") as %?%eq_sym; simplify_eq.
    destruct vs as [|v' vs'].
    { iDestruct "Hlist" as ">->".
      iDestruct (pointsto_agree with "Hlh Hlt") as %[=]. }
    iDestruct "Hlist" as (lh') "[>[Hlh'|[Hγd Hlh']] Hlist]".
    { by iCombine "Hlh Hlh'" gives %[]. }
    iDestruct (pointsto_agree with "Hlh Hlh'") as %?; simplify_eq.
    iCombine "Hlh Hlh'" as "Hlh"; iCombine "Hlhptr Hlhptr'" as "Hlhptr".
    wp_store. iDestruct "Hlhptr" as "[Hlhptr Hlhptr']".
    iMod ("Hupd" with "Hvs") as "[Hvs HΦ]".
    iMod ("Hclose" with "[Hvs Hlhptr' Hlt Hγe Hlist]") as "_".
    { iExists lh', lt', vs'. iNext. iFrame "Hvs Hlhptr' Hlt Hγe".
      destruct vs' as [|v'' vs'']=> //=.
      iDestruct "Hlist" as (l) "[Hlh Hlist]". auto with iFrame. }
    iModIntro. wp_seq. wp_apply (iron_wp_free with "Hlh"). iIntros "_"; wp_seq.
    wp_pures. iApply "HΦ". iExists lhptr, ltptr. eauto with iFrame.
  Qed.

  Lemma enqueue_spec E Φ q v γ :
    ↑N ⊆ E →
    is_queue N γ q -∗ enqueue_handle γ q -∗
    (∀ vs,
      γ ⤇{1/2} vs ={E∖↑N}=∗
      γ ⤇{1/2} (vs ++ [v]) ∗ ▷ (enqueue_handle γ q -∗ Φ #())) -∗
    WP enqueue q v @ E {{ Φ }}.
  Proof.
    iIntros (?) "Hq Hh Hupd". iDestruct "Hq" as (lhptr ltptr ->) "#?".
    iDestruct "Hh" as (?? lt ?%eq_sym)
      "(Hγinv & Hγe' & Hltptr)"; simplify_eq.
    wp_lam. wp_load. wp_alloc ltn as "Hltn"; wp_let. wp_pures. wp_bind (_ <- _)%E.
    iMod (fcinv_acc _ N with "[$] Hγinv") as "(H & Hγinv & Hclose)"; first done.
    iDestruct "H" as (lh lt' vs) "(>Hvs & >Hlhptr' & >Hγe & Hlist & >Hlt')".
    iCombine "Hγe Hγe'"
      gives %[<-%Excl_included%leibniz_equiv _]%auth_both_valid_discrete.
    iMod (own_update_2 with "Hγe Hγe'") as "[Hγe Hγe']".
    { by apply auth_update, option_local_update,
        (exclusive_local_update _ (Excl ltn)). }
    wp_store. iMod ("Hupd" with "Hvs") as "[Hvs HΦ]".
    iMod ("Hclose" with "[Hvs Hlhptr' Hγe Hlist Hlt' Hltn]") as "_".
    { iNext. iExists lh, ltn, (vs ++ [v]). iFrame "Hvs Hlhptr' Hγe Hltn".
      destruct vs as [|v' vs]; simpl.
      { iDestruct "Hlist" as "->". eauto with iFrame. }
      iDestruct "Hlist" as (l) "[Hlh Hlist]". iExists l; iFrame "Hlh".
      iInduction vs as [|w ws] "IH" forall (l); simpl.
      { iDestruct "Hlist" as "->". eauto with iFrame. }
      iDestruct "Hlist" as (l') "[Hl Hlist]". iExists l'. iFrame "Hl".
      by iApply ("IH" with "[$]"). }
    iModIntro. wp_seq. wp_store. iApply "HΦ".
    iExists lhptr, ltptr, ltn. iFrame; auto.
  Qed.

  Lemma delete_spec E Φ q γ :
    ↑N ⊆ E →
    is_queue N γ q -∗
    queue_cancel_own γ 1 -∗
    enqueue_handle γ q -∗ dequeue_handle γ q -∗
    γ ⤇{1/2} [] -∗
    (γ ⤇{1} [] ={E}=∗ ▷ Φ #()) -∗
    WP delete q @ E {{ Φ }}.
  Proof.
    iIntros (?) "Hq Hγc Heh Hdh Hvs HΦ". iDestruct "Hq" as (lhptr ltptr ->) "#?".
    iDestruct "Heh" as (?? lt ?%eq_sym) "(Hγinv & Hγe' & Hltptr)"; simplify_eq.
    iDestruct "Hdh" as (?? v' ?%eq_sym) "(Hγinv' & Hlhptr & Hγd)"; simplify_eq.
    iMod (fcinv_cancel with "[$] Hγc [$Hγinv $Hγinv']") as "H"; first done.
    iDestruct "H" as (lh lt' vs) "(>Hvs' & >Hlhptr' & >Hγe & Hlist & >Hlt)".
    iCombine "Hγe Hγe'"
      gives %[<-%Excl_included%leibniz_equiv _]%auth_both_valid_discrete.
    iDestruct (pointsto_agree with "Hlhptr Hlhptr'") as %?; simplify_eq.
    iCombine "Hlhptr Hlhptr'" as "Hlhptr".
    iDestruct (queue_own_agree with "Hvs Hvs'") as %<-; iCombine "Hvs Hvs'" as "Hvs".
    iDestruct "Hlist" as ">->".
    iMod ("HΦ" with "Hvs") as "HQ".
    wp_lam. wp_load. by do 3 wp_free.
  Qed.

  Lemma delete_all_spec E Φ Ψ (f : val) q γ (vs : list val) :
    ↑N ⊆ E →
    is_queue N γ q -∗
    queue_cancel_own γ 1 -∗
    enqueue_handle γ q -∗ dequeue_handle γ q -∗
    γ ⤇{1/2} vs -∗
    ▷ ([∗ list] v ∈ vs, WP f (of_val v) @ E {{ _, Ψ v }}) -∗
    ▷ (γ ⤇{1} [] -∗ ([∗ list] v ∈ vs, Ψ v) ={E}=∗ Φ #()) -∗
    WP delete_all f q @ E {{ Φ }}.
  Proof.
    iIntros (?) "#Hq Hγc Heh Hdh Hvs Hf HΦ".
    iInduction vs as [|v vs] "IH"; wp_lam; wp_pures.
    - wp_apply (dequeue_spec _ with "Hq Hdh"); first done.
      iIntros (vs) "Hvs'". iDestruct (queue_own_agree with "Hvs' Hvs") as %->.
      iIntros "!> {$Hvs'} !> Hdh /=". wp_let. wp_match.
      wp_apply (delete_spec with "Hq Hγc Heh Hdh Hvs"); first done.
      iIntros "Hvs". iMod ("HΦ" with "Hvs [//]") as "$"; auto.
    - iDestruct "Hf" as "[Hfv Hf]".
      wp_apply (dequeue_spec _ with "Hq Hdh"); first done.
      iIntros (vs') "Hvs'". iDestruct (queue_own_agree with "Hvs' Hvs") as %->.
      iMod (queue_own_update_2 _ _ _ vs with "Hvs Hvs'") as "[Hvs $]".
      iIntros "!> !> Hdh". wp_let. wp_match.
      wp_apply (iron_wp_wand with "Hfv"); iIntros (w) "HΨ"; wp_seq.
      iApply ("IH" with "Hγc Heh Hdh Hvs Hf"). iNext. iIntros "Hvs HΨs".
      iApply ("HΦ" with "[$]"). iFrame.
  Qed.
End queue_spec.

Global Opaque is_queue queue_cancel_own enqueue_handle dequeue_handle.

Record queue_bag_name := QueueBagName {
  queue_bag_queue_name : queue_name;
  queue_bag_inv_name : fcinv_name;
}.

Definition is_queue_bag `{!heapGS Σ, !queueG Σ} (N : namespace)
    (γ : queue_bag_name) (p : val) (Ψ : val → ironProp Σ) : ironProp Σ :=
  (is_queue (N .@ "queue") (queue_bag_queue_name γ) p ∗
  fcinv (N .@ "inv") (queue_bag_inv_name γ) (∃ xs,
    queue_bag_queue_name γ ⤇{1/2} xs ∗ [∗ list] x ∈ xs, Ψ x))%I.

Definition queue_bag_cancel_own `{heapGS Σ, queueG Σ}
    (γ : queue_bag_name) (p : frac) : ironProp Σ :=
  (queue_cancel_own (queue_bag_queue_name γ) p ∗
   fcinv_cancel_own (queue_bag_inv_name γ) p)%I.

Definition insert_handle `{heapGS Σ, queueG Σ}
    (γ : queue_bag_name) (q : val) : ironProp Σ :=
  (enqueue_handle (queue_bag_queue_name γ) q ∗
   fcinv_own (queue_bag_inv_name γ) (1/2))%I.

Definition remove_handle `{heapGS Σ, queueG Σ}
    (γ: queue_bag_name) (q : val) : ironProp Σ :=
  (dequeue_handle (queue_bag_queue_name γ) q ∗
   fcinv_own (queue_bag_inv_name γ) (1/2))%I.

Section queue_bag_spec.
  Context `{!heapGS Σ, !queueG Σ} (N : namespace) (Ψ : val → ironProp Σ).

  Global Instance is_queue_bag_uniform γ p : Uniform (is_queue_bag N γ p Ψ).
  Proof. apply _. Qed.
  Global Instance insert_handle_uniform γ q : Uniform (insert_handle γ q).
  Proof. apply _. Qed.
  Global Instance remove_handle_uniform γ q : Uniform (remove_handle γ q).
  Proof. apply _. Qed.
  Global Instance is_queue_bag_persistent γ γcinv p :
    Persistent (is_queue_bag γ γcinv p Ψ).
  Proof. apply _. Qed.

  Lemma new_queue_bag_spec E :
    ↑N ⊆ E →
    {{{ emp }}}
      new_queue #() @ E
    {{{ γ q, RET q;
      <affine> is_queue_bag N γ q Ψ ∗ queue_bag_cancel_own γ 1 ∗
      insert_handle γ q ∗ remove_handle γ q
    }}}.
  Proof.
    iIntros (? Φ) "_ HΦ". iApply iron_wp_fupd.
    iApply (new_queue_spec (N .@ "queue") with "[//]"); first solve_ndisj.
    iIntros "!>" (q γ) "(#Hq & Hc & Heh & Hdh & Hvs)".
    iMod (fcinv_alloc _ (N .@ "inv") (∃ xs, γ ⤇{1/2} xs ∗
      [∗ list] x ∈ xs, Ψ x)%I with "[Hvs]") as (γinv) "(#?&Hinvc&[Hγinv1 Hγinv2])".
    { iExists []; simpl; iFrame; auto. }
    iApply ("HΦ" $! (QueueBagName γ γinv)). iModIntro; iFrame. iSplitL; auto.
  Qed.

  Lemma bag_insert_spec E q γ v `{∀ x, Uniform (Ψ x)} :
    ↑N ⊆ E →
    {{{ is_queue_bag N γ q Ψ ∗ insert_handle γ q ∗ Ψ v }}}
      enqueue q v @ E
    {{{ RET #(); insert_handle γ q }}}.
  Proof.
    intros Hsub.
    iIntros (Φ) "[Hqueue [Hinsert HΨ]] HΦ".
    iDestruct "Hqueue" as "[#Hqueue #Hinv]".
    iDestruct "Hinsert" as "[Henqueue Hown_inv]".
    wp_apply (enqueue_spec (N .@ "queue") _ _ _ _ (queue_bag_queue_name γ)
      with "[] Henqueue"); eauto; first by solve_ndisj.
    iIntros (vs) "Hbag".
    iMod (fcinv_acc _ (N .@ "inv") with "[$] Hown_inv")
      as "(H & Hown_inv & Hclose)"; first by solve_ndisj.
    iDestruct "H" as (xs) "[>Hqueue_contents Hall]".
    iDestruct (queue_own_agree with "Hbag Hqueue_contents") as %->.
    iMod (queue_own_update_2 _ _ _ (xs ++ [v]) with "Hbag Hqueue_contents")
      as "[Hqueue_contents Hbag]".
    iMod ("Hclose" with "[Hqueue_contents Hall HΨ]") as "_".
    { iNext. iExists (xs ++ [v]); iFrame. by simpl. }
    iIntros "!> {$Hbag} !> Henqueue". iApply "HΦ"; iFrame.
  Qed.

  Lemma bag_remove_spec E q γ `{∀ x, Uniform (Ψ x)} :
    ↑N ⊆ E →
    {{{ is_queue_bag N γ q Ψ ∗ remove_handle γ q }}}
      dequeue q @ E
    {{{ v, RET v;
      ⌜v = NONEV⌝ ∧ remove_handle γ q ∨
      ∃ u, ⌜v = SOMEV u⌝ ∧ remove_handle γ q ∗ Ψ u
    }}}.
  Proof.
    iIntros (? Φ) "([#Hqueue #Hinv] & Hdequeue & Hown_inv) HΦ".
    wp_apply (dequeue_spec (N .@ "queue") with "[] Hdequeue");
      eauto; first by solve_ndisj.
    iIntros (vs) "Hbag".
    iMod (fcinv_acc _ (N .@ "inv") with "[$] Hown_inv")
      as "(H & Hinv_own & Hclose)"; first by solve_ndisj.
    iDestruct "H" as (xs) "[>Hqueue_contents Hall]".
    iDestruct (queue_own_agree with "Hbag Hqueue_contents") as %->.
    destruct xs as [|x xs]; simpl.
    - iMod ("Hclose" with "[Hqueue_contents]") as "_".
      { iNext. iExists _; iFrame. by simpl. }
      iIntros "!> {$Hbag} !> Hdequeue".
      iApply "HΦ"; iLeft; iFrame. by iSplit.
    - iDestruct "Hall" as "[HΨ Hall]".
      iMod (queue_own_update_2 _ _ _ (xs) with "Hbag Hqueue_contents")
        as "[Hqueue_contents Hbag]".
      iMod ("Hclose" with "[Hqueue_contents Hall]") as "_".
      { iNext. iExists _; iFrame. }
      iIntros "!> {$Hbag} !> Hdequeue".
      iApply "HΦ"; iRight; iFrame. iExists _; by iSplit.
  Qed.

  Lemma bag_delete_all_spec E q (f : val) γ `{∀ x, Uniform (Ψ x)} :
    ↑N ⊆ E →
    (∀ v, {{{ Ψ v }}} f v @ E {{{ v, RET v; emp }}}) -∗
    {{{
      is_queue_bag N γ q Ψ ∗ queue_bag_cancel_own γ 1 ∗
      insert_handle γ q ∗ remove_handle γ q
    }}}
      delete_all f q @ E
    {{{ RET #(); emp }}}.
  Proof.
    iIntros (?) "#Hf !#".
    iIntros (Φ) "([#Hqueue #Hi] & [Hcancel1 Hcancel2] &
      [[Henqueue Hown_inv] [ Hdequeue Hown_inv']]) HΦ".
    iMod (fcinv_cancel with "Hi Hcancel2 [$Hown_inv $Hown_inv']") as "H";
      eauto; first by solve_ndisj.
    iDestruct "H" as (xs) "[>Hbag Hall]".
    wp_apply (delete_all_spec (N .@ "queue") _ _ (λ _, emp%I)
      with "[] Hcancel1 Henqueue Hdequeue Hbag [Hall]"); eauto; first by solve_ndisj.
    { iNext. iInduction xs as [|x xs] "IH"=> //=.
      iDestruct "Hall" as "[HΨ Hall]".
      iSplitL "HΨ".
      iApply ("Hf" with "HΨ").
      iNext; eauto.
      iApply ("IH" with "Hall"). }
    iIntros "_ _ !>". by iApply "HΦ".
  Qed.
End queue_bag_spec.

Global Opaque is_queue_bag queue_bag_cancel_own insert_handle remove_handle.
