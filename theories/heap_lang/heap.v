(** This file defines the basic points-to [↦] connectives for the Iron logic
instantiated with the heap_lang language. As a counter part to this, it defines
the state interpretation [heap_ctx]. *)
From iris.bi.lib Require Import fractional.
From iris.proofmode Require Import proofmode.
From iron.proofmode Require Export fracpred.
From iris.algebra Require Import excl lib.frac_auth gmap agree ufrac lib.ufrac_auth.
From iris.base_logic.lib Require Export own.
From iris.base_logic.lib Require Export proph_map.
From iron.iron_logic Require Export weakestpre.
From iris.heap_lang Require Export lang.
Set Default Proof Using "Type".

Definition heapUR : ucmra := gmapUR loc (prodR fracR (agreeR valO)).
Definition to_heapUR : gmap loc val → heapUR :=
  fmap (λ v, (1%Qp, to_agree v)).

(** The camera we need. *)
Class heapGS Σ := HeapGS {
  heapG_invG : invGS Σ;
  heapG_inG :: inG Σ (ufrac_authR heapUR);
  heapG_name : gname;
  heapG_fcinv_cinvG : cinvG Σ;
  heapG_fcinv_inG : inG Σ (frac_authR ufracR);
  heapG_fork_post_name : gname;
  heapG_fork_postG :: inG Σ (authR (gmapUR positive (exclR (optionO ufracO))));
  heapG_proph_mapG :: proph_mapGS proph_id (val * val) Σ;
}.
Arguments heapG_name {_} _ : assert.
Arguments heapG_fork_post_name {_} _ : assert.

Definition heap_perm `{hG : heapGS Σ} (π : frac) : iProp Σ :=
  own (heapG_name hG) (◯U_π ε).
Global Instance perm_fractional `{hG : heapGS Σ} : Fractional heap_perm.
Proof. intros π1 π2. by rewrite -own_op -ufrac_auth_frag_op left_id. Qed.

Definition heapG_ironInvG `{hG : heapGS Σ} : ironInvG Σ := {|
  perm := heap_perm;
  fcinv_cinvG := heapG_fcinv_cinvG;
  fcinv_inG := heapG_fcinv_inG;
|}.

Definition heap_ctx `{hG : heapGS Σ} (h : gmap loc val) (n : nat) : iProp Σ :=
  ∃ πfs,
    ⌜ size πfs = n ⌝ ∧
    own (heapG_fork_post_name hG) (● (Excl <$> πfs : gmap _ (excl (option ufrac)))) ∗
    let πf : option fracR := [^op map] π ∈ πfs, π in
    own (heapG_name hG) (●U_(1%Qp ⋅? πf) (to_heapUR h)).
Definition heap_fork_post `{hG : heapGS Σ} : iProp Σ :=
  ∃ i π,
    from_option heap_perm True π ∗
    own (heapG_fork_post_name hG) (◯ {[ i := Excl π ]}).
Arguments heap_ctx : simpl never.

Global Instance heapG_ironG `{hG : heapGS Σ} : ironG heap_lang Σ := {|
  iron_invG := heapG_invG;
  iron_iron_invG := heapG_ironInvG;
  iron_state_interp σ κs n :=
    (heap_ctx (omap id σ.(heap)) n ∗ proph_map_interp κs σ.(used_proph_id))%I;
  iron_fork_post _ := heap_fork_post;
|}.

Section definitions.
  Context `{hG : heapGS Σ}.

  Definition pointsto_def (l : loc) (q : Qp) (v : val) : ironProp Σ :=
    FracPred (from_option
      (λ π, own (heapG_name hG) (◯U_π {[ l := (q, to_agree v) ]}))
      False)%I.
  Definition pointsto_aux : seal (@pointsto_def). by eexists. Qed.
  Definition pointsto := pointsto_aux.(unseal).
  Definition pointsto_eq : @pointsto = @pointsto_def := pointsto_aux.(seal_eq).
End definitions.

Notation "l ↦{ q } v" := (pointsto l q v)
  (at level 20, q at level 50, format "l  ↦{ q }  v") : bi_scope.
Notation "l ↦ v" := (pointsto l 1 v)
  (at level 20, format "l  ↦  v") : bi_scope.

Section to_heapUR.
  Implicit Types h : gmap loc val.
  Implicit Types v : val.

  (** Conversion to heaps and back *)
  Lemma to_heapUR_valid h : ✓ to_heapUR h.
  Proof. intros l. rewrite lookup_fmap. by case (h !! l)=> [[]|]. Qed.
  Lemma lookup_to_heapUR_None h l : h !! l = None → to_heapUR h !! l = None.
  Proof. by rewrite /to_heapUR lookup_fmap=> ->. Qed.
  Lemma heap_singleton_included h l q v :
    {[l := (q, to_agree v)]} ≼ to_heapUR h → h !! l = Some v.
  Proof.
    rewrite singleton_included_l=> -[[q' av] []].
    rewrite /to_heapUR lookup_fmap.
    rewrite fmap_Some_equiv=> -[v' /= [->]] [??]; setoid_subst.
    move=> /Some_pair_included_total_2 [_] /to_agree_included; naive_solver.
  Qed.
  Lemma to_heapUR_empty : to_heapUR ∅ = ∅.
  Proof. by rewrite /to_heapUR fmap_empty. Qed.
  Lemma to_heapUR_insert l v h :
    to_heapUR (<[l:=v]> h) = <[l:=(1%Qp, to_agree (v:leibnizO val))]> (to_heapUR h).
  Proof. by rewrite /to_heapUR fmap_insert. Qed.
  Lemma to_heapUR_delete l h :
    to_heapUR (delete l h) = delete l (to_heapUR h).
  Proof. by rewrite /to_heapUR fmap_delete. Qed.
  Global Instance to_heapUR_inj : Inj (=) (≡) to_heapUR.
  Proof.
    intros σ1 σ2. rewrite /to_heapUR=> Hσ. apply map_eq=> i.
    move: Hσ=> /(_ i). rewrite !lookup_fmap.
    case: (σ1 !! i)=> [v|]; case: (σ2 !! i)=> [v'|] /=; try by inversion 1.
    rewrite (inj_iff Some)=> -[_] /=.
    by rewrite (inj_iff to_agree) leibniz_equiv_iff=> ->.
  Qed.
End to_heapUR.

Section heap.
  Context `{heapGS Σ}.
  Implicit Types P Q : iProp Σ.
  Implicit Types Φ : val → iProp Σ.
  Implicit Types σ : gmap loc val.
  Implicit Types l : loc.
  Implicit Types v : val.

  (** General properties of pointsto *)
  Global Instance pointsto_uniform l q v : Uniform (l ↦{q} v).
  Proof.
    intros π1 π2. rewrite pointsto_eq /perm /pointsto_def /=.
    by rewrite -own_op -ufrac_auth_frag_op right_id.
  Qed.
  Global Instance pointsto_exist_perm l q v : ExistPerm (l ↦{q} v).
  Proof. by rewrite /ExistPerm pointsto_eq. Qed.

  Global Instance pointsto_timeless l q v : Timeless (l ↦{q} v).
  Proof. apply fracPred_at_timeless_alt. rewrite pointsto_eq. apply _. Qed.

  Global Instance pointsto_fractional l v : Fractional (λ q, l ↦{q} v)%I.
  Proof.
    intros p q. rewrite pointsto_eq /pointsto_def.
    iStartProof (iProp _); iIntros ([π|]) "/=".
    - iSplit.
      + iIntros "[??]". iExists (Some (π / 2)%Qp), (Some (π / 2)%Qp).	 iFrame.
        by rewrite -Some_op frac_op Qp.div_2.
      + iDestruct 1 as ([?|] [?|] Hπ) "[H1 H2]"; simpl; try done.
        move: Hπ. rewrite -Some_op. intros [=->]. by iSplitL "H1".
    - iSplit; first by iIntros "[]". by iDestruct 1 as ([?|] [?|] ?) "[??]".
  Qed.
  Global Instance pointsto_as_fractional l q v :
    AsFractional (l ↦{q} v) (λ q, l ↦{q} v)%I q.
  Proof. split. done. apply _. Qed.
  Global Instance frame_pointsto p l v q1 q2 q :
    FrameFractionalQp q1 q2 q →
    Frame p (l ↦{q1} v) (l ↦{q2} v) (l ↦{q} v) | 5.
  Proof. apply: frame_fractional. Qed.

  Lemma pointsto_agree l q1 q2 v1 v2 : l ↦{q1} v1 -∗ l ↦{q2} v2 -∗ ⌜v1 = v2⌝.
  Proof.
    iStartProof (iProp _). rewrite pointsto_eq /pointsto_def /=.
    iIntros ([π1|]) "H1"; iIntros ([π2|]) "H2 //=".
    iCombine "H1 H2" gives %Hv.
    move: Hv. rewrite -ufrac_auth_frag_op ufrac_auth_frag_valid=>Hv.
    move: Hv. rewrite singleton_op singleton_valid -pair_op.
    by intros [_ ?%to_agree_op_inv_L].
  Qed.

  Lemma pointsto_valid l q v : l ↦{q} v -∗ ⌜ ✓ q ⌝.
  Proof.
    iStartProof (iProp _). rewrite pointsto_eq /pointsto_def /=.
    iIntros ([π1|]) "H1 //=". iDestruct (own_valid with "H1") as %Hv.
    move: Hv. rewrite ufrac_auth_frag_valid=> Hv.
    move: Hv. by rewrite singleton_valid=> -[? _].
  Qed.
  Lemma pointsto_valid_2 l q1 q2 v1 v2 :
    l ↦{q1} v1 -∗ l ↦{q2} v2 -∗ ⌜ ✓ (q1 + q2)%Qp ⌝.
  Proof.
    iIntros "H1 H2". iDestruct (pointsto_agree with "H1 H2") as %->.
    iApply (pointsto_valid l _ v2). by iFrame.
  Qed.

  Global Instance pointsto_combine_sep_gives l q1 q2 v1 v2 :
    CombineSepGives (l ↦{q1} v1) (l ↦{q2} v2) ⌜✓ (q1 + q2)%Qp ∧ v1 = v2⌝.
  Proof.
    rewrite /CombineSepGives. iIntros "[H1 H2]". iSplit.
    - iDestruct (pointsto_valid_2 with "H1 H2") as %?; auto.
    - iDestruct (pointsto_agree with "H1 H2") as %?; auto.
  Qed.

  Lemma big_pointsto_None σ :
    ([∗ map] l ↦ v ∈ σ, l ↦ v) None ⊢ ⌜ σ = ∅ ⌝.
  Proof.
    iIntros "Hσ". destruct (decide (σ = ∅)); first done.
    destruct (map_choose σ) as (k & w & ?); first done.
    rewrite big_sepM_lookup_acc; last done. rewrite fracPred_at_sep.
    by iDestruct "Hσ" as ([π3|] [π4|] ?) "[? _]".
  Qed.

  Lemma big_pointsto_alt_1 h π :
    ([∗ map] l ↦ v ∈ h, l ↦ v) (Some π) ⊢ own (heapG_name H) (◯U_π (to_heapUR h)).
  Proof.
    revert π. induction h as [|l v h Hl IH] using map_ind=> π.
    { rewrite big_sepM_empty fracPred_at_emp. iIntros ([=]). }
    rewrite big_sepM_insert // to_heapUR_insert fracPred_at_sep.
    rewrite insert_singleton_op; last by apply lookup_to_heapUR_None.
    iDestruct 1 as ([π1|] [π2|] Hπ) "[Hl Hσ]"=>//.
    - rewrite IH // pointsto_eq. apply (inj Some) in Hπ as ->. by iSplitL "Hl".
    - iDestruct (big_pointsto_None with "Hσ") as %->. apply (inj Some) in Hπ as ->.
      by rewrite to_heapUR_empty right_id pointsto_eq.
  Qed.
  Lemma big_pointsto_alt_2 h π :
    h ≠ ∅ →
    own (heapG_name H) (◯U_π (to_heapUR h)) ⊢ ([∗ map] l ↦ v ∈ h, l ↦ v) (Some π).
  Proof.
    revert π. induction h as [|l v h Hl IH] using map_ind; [done|intros π _].
    rewrite to_heapUR_insert. destruct (decide (h = ∅)) as [->|].
    { by rewrite to_heapUR_empty insert_empty big_sepM_singleton pointsto_eq. }
    rewrite big_sepM_insert // fracPred_at_sep.
    rewrite insert_singleton_op; last by apply lookup_to_heapUR_None.
    iIntros "[Hl Hh]". iExists (Some (π / 2))%Qp, (Some (π / 2))%Qp.
    iSplit; first by rewrite -Some_op frac_op Qp.div_2.
    rewrite IH // pointsto_eq. by iSplitL "Hl".
  Qed.

  (* heap_ctx manipulating lemmas *)
  Lemma heap_thread_adequacy h h' n π1 π2 :
    π1 ⋅? π2 = 1%Qp →
    heap_ctx h n -∗
    [∗] replicate n heap_fork_post -∗
    heap_perm π1 -∗
    ([∗ map] l↦v ∈ h', l ↦ v) π2 ==∗
    ⌜ h = h' ⌝.
  Proof.
    iIntros (Hπ). iDestruct 1 as (πfs <-) "[Hn Hh]". iIntros "HQs Hp Hh'".
    iAssert (own (heapG_name H) (◯U_(1%Qp ⋅? ([^op map] π ∈ πfs, π)) (to_heapUR h')))%I
      with "[> Hn HQs Hp Hh']" as "Hh'"; last first.
    { by iCombine "Hh Hh'" gives %?%ufrac_auth_agree%(inj _). }
    iInduction (map_wf πfs) as [πfs _] "IH".
    destruct (size πfs) as [|m'] eqn:Hsize; simpl.
    { apply map_size_empty_iff in Hsize as ->.
      rewrite big_opM_empty /=. destruct π2 as [π2|]; simpl in *; last first.
      - iDestruct (big_pointsto_None with "Hh'") as %->.
        by rewrite to_heapUR_empty -Hπ.
      - iDestruct (big_pointsto_alt_1 with "Hh'") as "Hh'".
        iCombine "Hp Hh'" as "Hh'". by rewrite left_id -Hπ. }
    iDestruct "HQs" as "[HQ HQs]". iDestruct "HQ" as (j π) "[Hp' Hj]".
    iAssert ⌜ πfs !! j = Some π ⌝%I as %Hj.
    { iDestruct (own_valid_2 with "Hn Hj")
        as %[Hj%singleton_included_l Hvalid]%auth_both_valid_discrete.
      destruct Hj as (eπ'&Hj%leibniz_equiv&[[=]|(?&?&[= <-]&[= <-]&Heπ)]%option_included).
      destruct Heπ as [<-%leibniz_equiv|Hincl]; last first.
      { exfalso. move: (Hvalid j). rewrite Hj. destruct eπ'=> //.
        by destruct Hincl as [[] ?%leibniz_equiv]. }
      move: Hj; rewrite lookup_fmap fmap_Some=> -[π' [Hj ?]]; by simplify_eq. }
    rewrite (big_opM_delete (o:=op) _ πfs) //.
    iMod (own_update_2 with "Hn Hj") as "Hn".
    { apply auth_update_dealloc, delete_singleton_local_update, _. }
    rewrite -fmap_delete.
    iMod ("IH" with "[%] Hn [HQs] Hp Hh'") as "Hh'".
    { apply delete_subset; eauto. }
    { replace m' with (size (delete j πfs)); first done.
      apply (inj S). rewrite -Hsize -{2}(insert_delete πfs j π) //.
      by rewrite map_size_insert ?lookup_delete. }
    destruct π as [π|]; last by rewrite left_id_L.
    iCombine "Hp' Hh'" as "Hh'". rewrite left_id.
    by rewrite -cmra_opM_opM_assoc_L /= -(comm_L _ π) cmra_op_opM_assoc_L.
  Qed.

  Lemma heap_thread_alloc h n π :
    heap_ctx h n ==∗ ∃ i,
      heap_ctx h (S n) ∗
      from_option perm True π ∗
      own (heapG_fork_post_name _) (◯ {[ i := Excl π ]}).
  Proof.
    iDestruct 1 as (πfs Hsize) "[Hn Hh]".
    set (i := fresh (dom πfs)).
    assert (πfs !! i = None) by (by eapply (not_elem_of_dom (D:=gset positive)), is_fresh).
    iMod (own_update with "Hn") as "[Hn Hi]".
    { apply auth_update_alloc, (alloc_singleton_local_update _ i (Excl π))=> //.
      by rewrite lookup_fmap fmap_None. }
    iExists i. iFrame "Hi".
    destruct π as [π|]; last first.
    { iModIntro. iSplitL=> //. iExists (<[i:=None]> πfs).
      iSplit; first by rewrite map_size_insert_None // Hsize.
      rewrite fmap_insert. iFrame "Hn". by rewrite big_opM_insert // left_id. }
    iMod (own_update with "Hh") as "[Hh Hp]".
    { apply (ufrac_auth_update_surplus _ π _ (ε : heapUR)).
      rewrite right_id. apply to_heapUR_valid. }
    rewrite right_id. iModIntro. iFrame "Hp".
    iExists (<[i:=Some π]> πfs).
    iSplit; first (by rewrite map_size_insert_None // Hsize).
    rewrite big_opM_insert // (comm op).
    rewrite -cmra_opM_opM_assoc_L fmap_insert. iFrame.
  Qed.

  Lemma heap_alloc h n l v π :
    h !! l = None →
    heap_ctx h n -∗ perm π ==∗
    heap_ctx (<[l:=v]> h) n ∗ (l ↦ v) (Some π).
  Proof.
    iIntros (?). rewrite /heap_ctx /perm pointsto_eq /pointsto_def /=.
    iDestruct 1 as (πfs Hsize) "[Hπfs Hh]"; iIntros "Hp".
    iMod (own_update_2 with "Hh Hp") as "[Hh Hl]".
    { eapply ufrac_auth_update,
        (alloc_singleton_local_update _ _ (1%Qp, to_agree (v:leibnizO _))) => //.
      by apply lookup_to_heapUR_None. }
    iModIntro. iFrame "Hl". iExists πfs. rewrite to_heapUR_insert. by iFrame.
  Qed.

  Lemma heap_alloc_big h h' n π :
    h' ##ₘ h → h' ≠ ∅ →
    heap_ctx h n -∗ perm π ==∗
    heap_ctx (h' ∪ h) n ∗ ([∗ map] l ↦ v ∈ h', l ↦ v) (Some π).
  Proof.
    revert h π; induction h' as [|l v h' Hl IH] using map_ind;
      iIntros (h π ? Hempty) "Hh Hπ"; decompose_map_disjoint; [done|].
    destruct (decide (h' = ∅)) as [->|].
    { iMod (heap_alloc _ _ _ v with "Hh Hπ") as "[Hh Hl]"; first done.
      rewrite !insert_empty -insert_union_singleton_l big_sepM_singleton.
      by iFrame. }
    rewrite !big_opM_insert // -insert_union_l //.
    iDestruct "Hπ" as "[Hπ1 Hπ2]".
    iMod (IH with "Hh Hπ1") as "[Hh Hh']"; [done..|].
    iMod (heap_alloc _ _ _ v with "Hh Hπ2") as "[$ Hl]";
      first by apply lookup_union_None.
    iModIntro. rewrite fracPred_at_sep. iExists _, _. iFrame.
    by rewrite -Some_op frac_op Qp.div_2.
  Qed.

  Lemma heap_dealloc h n l v π :
    heap_ctx h n -∗
    (l ↦ v) (Some π) ==∗
    heap_ctx (delete l h) n ∗ perm π.
  Proof.
    rewrite /heap_ctx /perm pointsto_eq /pointsto_def /=.
    iDestruct 1 as (πfs Hsize) "[Hπfs Hh]"; iIntros "Hl".
    iDestruct (own_valid_2 with "Hh Hl")
      as %Hl%ufrac_auth_included_total%heap_singleton_included.
    rewrite to_heapUR_delete. iMod (own_update_2 with "Hh Hl") as "[Hh Hl]".
    { eapply ufrac_auth_update,
        (delete_singleton_local_update_cancelable _ _ _).
      by rewrite /to_heapUR lookup_fmap Hl. }
    iModIntro. iFrame "Hl". iExists πfs. by iFrame.
  Qed.

  Lemma heap_valid h n l q v π :
    heap_ctx h n -∗ (l ↦{q} v) π -∗ ⌜h !! l = Some v⌝.
  Proof.
    rewrite /heap_ctx /perm pointsto_eq /pointsto_def /=.
    iDestruct 1 as (πfs Hsize) "[Hπfs Hh]"; iIntros "Hl". destruct π as [π|]=>//.
    by iDestruct (own_valid_2 with "Hh Hl")
      as %Hl%ufrac_auth_included%Some_included_total%heap_singleton_included.
  Qed.

  Lemma heap_update h n l v1 v2 π :
    heap_ctx h n -∗ (l ↦ v1) π ==∗
    heap_ctx (<[l:=v2]> h) n ∗ (l ↦ v2) π.
  Proof.
    rewrite /heap_ctx /perm pointsto_eq /pointsto_def /=.
    iDestruct 1 as (πfs Hsize) "[Hπfs Hh]"; iIntros "Hl". destruct π as [π|]=>//.
    iDestruct (own_valid_2 with "Hh Hl")
      as %Hl%ufrac_auth_included%Some_included_total%heap_singleton_included.
    iMod (own_update_2 with "Hh Hl") as "[Hh Hl]".
    { eapply ufrac_auth_update, singleton_local_update,
        (exclusive_local_update _ (1%Qp, to_agree v2))=> //.
      by rewrite /to_heapUR lookup_fmap Hl. }
    iModIntro. iFrame "Hl". iExists πfs. rewrite to_heapUR_insert. by iFrame.
  Qed.
End heap.
